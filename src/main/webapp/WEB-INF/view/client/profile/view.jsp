<%@ include file="/WEB-INF/view/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/view/jspf/directive/taglib.jspf"%>
<html>
<%@ include file="/WEB-INF/view/jspf/head.jspf"%>
<link href="<c:url value="/static/style/profile.css" />" rel="stylesheet"/>
<%--<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/style/profile.css"/>--%>
<body>
	<%@ include file="/WEB-INF/view/jspf/header.jspf"%>

	<div class="page-content page-container" id="page-content">
		<div class="padding">
			<div class="row">
				<div class="col-xl-12 col-md-12">
					<div class="card user-card-full">
						<div class="row m-l-0 m-r-0">
							<div class="col-sm-4 bg-c-lite-green user-profile">
								<div class="card-block text-center text-white">
									<div class="m-b-25"> <img src="https://img.icons8.com/bubbles/100/000000/user.png" class="img-radius" alt="User-Profile-Image"> </div>
									<p>
										<fmt:message key="profile.view_jsp.label.full_name" bundle="${langBundle}"/>
									</p>
									<h6 class="f-w-600"><c:out value="${requestScope.full_name}"></c:out></h6> <i class=" mdi mdi-square-edit-outline feather icon-edit m-t-10 f-16"></i>
								</div>
							</div>
							<div class="col-sm-8">
								<div class="card-block">
									<h6 class="m-b-20 p-b-5 b-b-default f-w-600">
										<fmt:message key="header_jspf.anchor.profile" bundle="${langBundle}"/>
									</h6>
									<div class="row">
										<div class="col-sm-6">
											<p class="m-b-10 f-w-600"><fmt:message key="profile.view_jsp.label.email" bundle="${langBundle}"/></p>
											<h6 class="text-muted f-w-400"><c:out value="${requestScope.email}"></c:out></h6>
										</div>
										<div class="col-sm-6">
											<p class="m-b-10 f-w-600"><fmt:message key="profile.view_jsp.label.attachment" bundle="${langBundle}"/></p>
											<h6 class="text-muted f-w-400">
												<c:if test="${not empty requestScope.attachment}">
													<a target="_blank" href="<c:out value="${requestScope.attachment}"></c:out>">
														<fmt:message key="profile.view_jsp.label.attachment" bundle="${langBundle}"/>
													</a>
												</c:if>

											</h6>
										</div>
									</div>
									<h6 class="m-b-20 m-t-40 p-b-5 b-b-default f-w-600"></h6>
									<div class="row">
										<div class="col-sm-6">
											<p class="m-b-10 f-w-600"><fmt:message key="profile.view_jsp.label.city" bundle="${langBundle}"/></p>
											<h6 class="text-muted f-w-400"><c:out value="${requestScope.city}"></c:out></h6>
										</div>
										<div class="col-sm-6">
											<p class="m-b-10 f-w-600"><fmt:message key="profile.view_jsp.label.region" bundle="${langBundle}"/></p>
											<h6 class="text-muted f-w-400"><c:out value="${requestScope.region}"></c:out></h6>
										</div>
									</div>
									<h6 class="m-b-20 m-t-40 p-b-5 b-b-default f-w-600"></h6>
									<div class="row">
										<div class="col-sm-6">
											<p class="m-b-10 f-w-600"><fmt:message key="profile.view_jsp.label.educational_institution" bundle="${langBundle}"/></p>
											<h6 class="text-muted f-w-400"><c:out value="${requestScope.educational_institution}"></c:out></h6>
										</div>
										<div class="col-sm-3">
											<p class="m-b-10 f-w-600">
												<input type="button" onclick="location.href='controller?command=editProfile&show=true&user=${requestScope.email}';"
													   value="<fmt:message key="profile.view_jsp.button.edit" bundle="${langBundle}"/>" class="form-control input-sm">
											</p>
											<p class="text-muted f-w-400">
												<form action="controller" method="POST">
													<input type="hidden" name="command" value="logout">
													<input type="submit" class="form-control input-sm" value="<fmt:message key="profile.view_jsp.button.logout" bundle="${langBundle}"/>">
												</form>
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<%@ include file="/WEB-INF/view/jspf/footer.jspf" %>
</body>
</html>