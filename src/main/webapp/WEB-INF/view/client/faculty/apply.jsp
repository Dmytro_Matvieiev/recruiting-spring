<%@ include file="/WEB-INF/view/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/view/jspf/directive/taglib.jspf"%>
<html>
<%@ include file="/WEB-INF/view/jspf/head.jspf"%>
<link href="<c:url value="/static/style/faculty.css" />" rel="stylesheet"/>
<%--<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/style/faculty.css"/>--%>
<body>
	<%@ include file="/WEB-INF/view/jspf/header.jspf"%>


	<main class="apply-faculty">
		<div class="cotainer">
			<div class="row justify-content-center">
				<div class="col-md-8">
					<div class="card">
						<div class="card-header">
							<div class="header">
								<c:out value="${lang eq 'ru' ? name_ru : name_eng}"></c:out>
<%--								<c:out value="${language eq 'ru' ? name_ru : name_eng}"></c:out>--%>
							</div>
						</div>
						<div class="card-body">
							<div class="form-group row">
								<label class="col-md-4 col-form-label text-md-right">
									<fmt:message key="faculty.apply_jsp.label.name" bundle="${langBundle}"/>
								</label>
								<div class="col-md-6">
									<c:out value="${lang eq 'ru' ? name_ru : name_eng}"></c:out>
								</div>
							</div>

							<div class="form-group row">
								<label class="col-md-4 col-form-label text-md-right">
									<fmt:message key="faculty.apply_jsp.label.total_places" bundle="${langBundle}"/>
								</label>
								<div class="col-md-6">
									<c:out value="${total_places}"></c:out>
								</div>
							</div>

							<div class="form-group row">
								<label class="col-md-4 col-form-label text-md-right">
									<fmt:message key="faculty.apply_jsp.label.budget_places" bundle="${langBundle}"/>
								</label>
								<div class="col-md-6">
									<c:out value="${budget_places}"></c:out>
								</div>
							</div>

							<div class="form-group col-md-8 offset-md-2">
								<form action="controller" method="POST">
									<input type="hidden" name="command" value="applyFaculty" />
									<input type="hidden" name="id" value="${id}" />

									<fieldset class="the-fieldset">
										<legend class="the-legend"><fmt:message key="faculty.apply_jsp.label.entrance_subjects" bundle="${langBundle}"/></legend>
										<c:if test="${empty facultySubjects}">
											<fmt:message key="faculty.apply_jsp.label.no_entrance_subjects_msg" bundle="${langBundle}"/>
										</c:if>

										<c:forEach var="facultySubject" items="${facultySubjects}">
											<div class="form-group row">
												<label class="col-md-4 col-form-label text-md-right">
													<c:out value="${lang eq 'ru' ? facultySubject.nameRu : facultySubject.nameEng}"></c:out>
												</label>
												<div class="col-md-6">
													<grades:insert subjectId="${facultySubject.id}" examType="entrance" />
												</div>
											</div>
										</c:forEach>
									</fieldset>
									<fieldset class="the-fieldset">
										<legend class="the-legend"><fmt:message key="faculty.apply_jsp.label.diploma_subjects" bundle="${langBundle}"/></legend>
										<c:forEach var="subject" items="${allSubjects}">
											<div class="form-group row">
												<label class="col-md-4 col-form-label text-md-right">
                                                    <c:out value="${lang eq 'ru' ? subject.nameRu : subject.nameEng}"></c:out>
												</label>
												<div class="col-md-6">
													<grades:insert subjectId="${subject.id}" examType="diploma" />
												</div>
											</div>
										</c:forEach>
										<div class="form-group row">
											<div class="col-md-6 offset-md-4">
												<input type="submit" class="btn btn-primary btn-block"
													   value="<fmt:message key="faculty.apply_jsp.button.submit" bundle="${langBundle}"/>">
											</div>
										</div>
									</fieldset>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</main>

	<%@ include file="/WEB-INF/view/jspf/footer.jspf" %>
</body>
</html>