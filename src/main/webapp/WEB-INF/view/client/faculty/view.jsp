<%@ include file="/WEB-INF/view/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/view/jspf/directive/taglib.jspf"%>
<html>
<%@ include file="/WEB-INF/view/jspf/head.jspf"%>
<body>
	<%@ include file="/WEB-INF/view/jspf/header.jspf"%>
	<main class="view-faculty">
		<div class="cotainer">
			<div class="row justify-content-center">
				<div class="col-md-8">
					<div class="card">
						<div class="card-header">
							<div class="header">
								<c:out value="${language eq 'ru' ? name_ru : name_eng}"></c:out>
							</div>
						</div>
						<div class="card-body">
							<div class="form-group row">
								<label class="col-md-4 col-form-label text-md-right">
									<fmt:message key="faculty.view_jsp.label.name" bundle="${langBundle}"/>
								</label>
								<div class="col-md-6">
									<c:out value="${language eq 'ru' ? name_ru : name_eng}"></c:out>
								</div>
							</div>

							<div class="form-group row">
								<label class="col-md-4 col-form-label text-md-right">
									<fmt:message key="faculty.view_jsp.label.total_places" bundle="${langBundle}"/>
								</label>
								<div class="col-md-6">
									<c:out value="${total_places}"></c:out>
								</div>
							</div>

							<div class="form-group row">
								<label class="col-md-4 col-form-label text-md-right">
									<fmt:message key="faculty.view_jsp.label.budget_places" bundle="${langBundle}"/>
								</label>
								<div class="col-md-6">
									<c:out value="${budget_places}"></c:out>
								</div>
							</div>

							<div class="form-group row">
								<label class="col-md-4 col-form-label text-md-right">
									<fmt:message key="faculty.view_jsp.label.entrance_subjects" bundle="${langBundle}"/>
								</label>
								<div class="col-md-6">
									<c:if test="${empty facultySubjects}">
										<fmt:message key="faculty.view_jsp.label.no_subjects_msg" bundle="${langBundle}"/>
									</c:if>

									<c:if test="${not empty facultySubjects}">
										<ul>
											<c:forEach var="subject" items="${facultySubjects}">
												<li><c:out
														value="${language eq 'ru' ? subject.nameRu : subject.nameEng}"></c:out></li>
											</c:forEach>
										</ul>
									</c:if>
								</div>
							</div>

							<div class="form-group row">
								<div class="col-md-6 offset-md-4">
									<c:if test="${userRole eq 'client' }">
										<input type="button"
											   onclick="location.href='controller?command=applyFaculty&name_eng=${name_eng}';"
											   value="<fmt:message key="faculty.view_jsp.button.apply" bundle="${langBundle}"/>"
											   class="btn btn-primary btn-block">
									</c:if>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>

	</main>
	<%@ include file="/WEB-INF/view/jspf/footer.jspf" %>
</body>
</html>