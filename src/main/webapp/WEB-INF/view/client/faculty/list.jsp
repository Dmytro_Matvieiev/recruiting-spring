<%@ include file="/WEB-INF/view/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/view/jspf/directive/taglib.jspf"%>
<html>
<%@ include file="/WEB-INF/view/jspf/head.jspf"%>
<link href="<c:url value="/static/style/faculty.css" />" rel="stylesheet"/>
<%--<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/style/faculty.css"/>--%>
<body>
	<%@ include file="/WEB-INF/view/jspf/header.jspf"%>

	<main class="client-faculty">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header">
							<div class="header">
								<fmt:message key="faculty.list_jsp.label.faculties_list" bundle="${langBundle}"/>
							</div>
						</div>
						<div class="card-body">
							<table id="facultiesTable" class="display">
								<thead>
								<tr>
									<td><fmt:message key="faculty.list_jsp.label.name" bundle="${langBundle}"/></td>
									<td><fmt:message key="faculty.list_jsp.label.total_places" bundle="${langBundle}"/></td>
									<td><fmt:message key="faculty.list_jsp.label.budget_places" bundle="${langBundle}"/></td>
								</tr>
								</thead>
								<tbody>
								<c:forEach var="faculty" items="${faculties}">
									<tr>
										<td>
											<a href="<c:url value="controller?command=viewFaculty"> <c:param name="name_eng" value="${faculty.nameEng}"/></c:url>">
												<c:out value="${language eq 'ru' ? faculty.nameRu : faculty.nameEng}"></c:out>
											</a>
										</td>
										<td><c:out value="${faculty.totalPlaces}"></c:out></td>
										<td><c:out value="${faculty.budgetPlaces}"></c:out></td>
									</tr>
								</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</main>

	<%@ include file="/WEB-INF/view/jspf/footer.jspf" %>
	<script type="text/javascript">
		var language = "${language}";
		$(document).ready(function() {
			$('#facultiesTable').dataTable({
				destroy: true,
				orderCellsTop: true,
				aaSorting: [],
				lengthMenu: [[5, 10, 25], [5, 10, 25]],
				// "scrollX": true,
				fixedHeader: true,
				dom: 'lBfrtip',
				// "scrollY":        "60vh",
				"scrollCollapse": true,
				ordering: true,
				buttons: [
					'copy', 'excel'
				],
				"language" : {
					"url" : (language == 'ru') ? "../static/js/dataTables/russian.lang" : "",
				}
			});
		});
	</script>

	<%@ include file="/WEB-INF/view/jspf/footer.jspf" %>
</body>
</html>