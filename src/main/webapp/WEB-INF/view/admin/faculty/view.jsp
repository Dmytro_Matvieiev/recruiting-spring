<%@ include file="/WEB-INF/view/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/view/jspf/directive/taglib.jspf"%>
<html>
<%@ include file="/WEB-INF/view/jspf/head.jspf"%>
<link href="<c:url value="/static/style/faculty.css" />" rel="stylesheet"/>
<body>
	<%@ include file="/WEB-INF/view/jspf/header.jspf"%>

	<main class="view-faculty">
		<div class="cotainer">
			<div class="row justify-content-center">
				<div class="col-md-8">
					<div class="card">
						<div class="card-header">
							<div class="header">
								<fmt:message key="faculty.view_jsp.label.title" bundle="${langBundle}"/>
							</div>
						</div>
						<div class="card-body">
							<div class="form-group row">
								<label class="col-md-4 col-form-label text-md-right">
									<fmt:message key="faculty.view_jsp.label.name" bundle="${langBundle}"/>
								</label>
								<div class="col-md-6">
									<c:out value="${language eq 'ru' ? name_ru : name_eng}"></c:out>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-md-4 col-form-label text-md-right">
									<fmt:message key="faculty.view_jsp.label.total_places" bundle="${langBundle}"/>
								</label>
								<div class="col-md-6">
									<c:out value="${total_places}"></c:out>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-md-4 col-form-label text-md-right">
									<fmt:message key="faculty.view_jsp.label.budget_places" bundle="${langBundle}"/>
								</label>
								<div class="col-md-6">
									<c:out value="${budget_places}"></c:out>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-md-4 col-form-label text-md-right">
									<fmt:message key="faculty.view_jsp.label.entrance_subjects" bundle="${langBundle}"/>
								</label>
								<div class="col-md-6">
									<c:if test="${empty facultySubjects}">
										<fmt:message key="faculty.view_jsp.label.no_subjects_msg" bundle="${langBundle}"/>
									</c:if>

									<c:if test="${not empty facultySubjects}">
										<ol>
											<c:forEach var="subject" items="${facultySubjects}">
												<li>
													<a href="<c:url value="controller?command=viewSubject">
														<c:param name="name_eng" value="${subject.nameEng}"/></c:url>">
														<c:out value="${lang eq 'ru' ? subject.nameRu : subject.nameEng}"></c:out>
													</a>
												</li>
											</c:forEach>
										</ol>
									</c:if>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-md-4 col-form-label text-md-right">
									<fmt:message key="faculty.view_jsp.label.faculty_enrollees" bundle="${langBundle}"/>
								</label>
								<div class="col-md-6">
									<c:if test="${empty facultyEnrollees}">
										<fmt:message key="faculty.view_jsp.label.no_faculty_enrollees_msg" bundle="${langBundle}"/>
									</c:if>

									<c:if test="${not empty facultyEnrollees}">
										<ol>
											<c:forEach var="enrollee" items="${facultyEnrollees}">
												<li>
													<a href="<c:url value="controller?command=viewEnrollee">
														<c:param name="userId" value="${enrollee.key.userId}"/> </c:url>">
														<c:out value="${enrollee.value}"></c:out>
													</a>
												</li>
											</c:forEach>
										</ol>
									</c:if>
								</div>
							</div>
							<div class="form-group row">
								<div class="col-md-6 offset-md-4">
									<input type="button" onclick="location.href='controller?command=editFaculty&name_eng=${requestScope.name_eng}';"
										   value="<fmt:message key="faculty.view_jsp.button.edit" bundle="${langBundle}"/>" class="btn btn-primary btn-block">
								</div>
							</div>
							<div class="form-group row">
								<div class="col-md-6 offset-md-4">
									<input type="button" onclick="location.href='controller?command=createReport&id=${id}';"
										   value="<fmt:message key="faculty.view_jsp.button.create_report" bundle="${langBundle}"/>" class="btn btn-primary btn-block">
								</div>
							</div>
							<div class="form-group row">
								<div class="col-md-6 offset-md-4">
									<form id="delete_faculty" action="controller" method="POST">
										<input type="hidden" name="command" value="deleteFaculty" />
										<input type="hidden" name="id" value="${id}" />
										<input type="submit" value="<fmt:message key="faculty.view_jsp.button.delete" bundle="${langBundle}"/>" class="btn btn-primary btn-block">
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<%--		<c:if test="${not empty errorMessage}">--%>
		<%--			<c:out value="${errorMessage}"></c:out>--%>
		<%--		</c:if>--%>

		<%--		<c:if test="${not empty successfulMessage}">--%>
		<%--			<c:out value="${successfulMessage}"></c:out>--%>
		<%--		</c:if>--%>

	</main>
	<%@ include file="/WEB-INF/view/jspf/footer.jspf" %>
</body>
</html>