<%@ include file="/WEB-INF/view/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/view/jspf/directive/taglib.jspf"%>
<html>
<%@ include file="/WEB-INF/view/jspf/head.jspf"%>
<link href="<c:url value="/static/style/faculty.css" />" rel="stylesheet"/>
<%--<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/style/faculty.css"/>--%>
<body>
	<%@ include file="/WEB-INF/view/jspf/header.jspf"%>

	<main class="add-faculty">
		<div class="cotainer">
			<div class="row justify-content-center">
				<div class="col-md-8">
					<div class="card">
						<div class="card-header">
							<div class="header">
								<fmt:message key="faculty.add_jsp.label.title" bundle="${langBundle}"/>
							</div>
						</div>
						<div class="card-body">
							<form id="add_faculty" action="controller" method="POST" autocomplete="off" onsubmit="return validate();">
								<input type="hidden" name="command" value="addFaculty" />
								<div class="form-group row">
									<label for="name_ru" class="col-md-4 col-form-label text-md-right">
										<fmt:message key="faculty.add_jsp.label.name" bundle="${langBundle}"/> (ru)
									</label>
									<div class="col-md-6">
										<input type="text" name="name_ru" id="name_ru" class="form-control" required />
									</div>
								</div>
								<div class="form-group row">
									<label for="name_eng" class="col-md-4 col-form-label text-md-right">
										<fmt:message key="faculty.add_jsp.label.name" bundle="${langBundle}"/> (eng)
									</label>
									<div class="col-md-6">
										<input type="text" name="name_eng" id="name_eng" class="form-control" required />
									</div>
								</div>
								<div class="form-group row">
									<label for="total_places" class="col-md-4 col-form-label text-md-right">
										<fmt:message key="faculty.add_jsp.label.total_places" bundle="${langBundle}"/>
									</label>
									<div class="col-md-6">
										<input type="number" name="total_places" id="total_places" class="form-control"  min="1" max="800" step="1" required />
									</div>
								</div>
								<div class="form-group row">
									<label for="budget_places" class="col-md-4 col-form-label text-md-right">
										<fmt:message key="faculty.add_jsp.label.budget_places" bundle="${langBundle}"/>
									</label>
									<div class="col-md-6">
										<input type="number" name="budget_places" id="budget_places" class="form-control"  min="1" max="799" step="1" required />
									</div>
								</div>
								<div class="form-group row">
									<label class="col-md-4 col-form-label text-md-right">
										<fmt:message key="faculty.add_jsp.label.entrance_subjects" bundle="${langBundle}"/>
									</label>
									<div class="col-md-6">
										<c:forEach var="subject" items="${allSubjects}">
											<p>
												<input type="checkbox" name="subjects" value="${subject.id}" />
												<c:out value="${lang eq 'ru' ? subject.nameRu : subject.nameEng}"></c:out>
											</p>
										</c:forEach>
									</div>
								</div>
								<div class="form-group row">
									<div class="col-md-6 offset-md-4">
										<input type="submit" value="<fmt:message key="faculty.add_jsp.button.submit" bundle="${langBundle}"/>" class="btn btn-primary btn-block">
									</div>
								</div>
								<div class="form-group row">
									<div class="col-md-6 offset-md-4">
										<input type="button" onclick="location.href='controller?command=viewAllFaculties';"
											   value="<fmt:message key="faculty.add_jsp.button.back" bundle="${langBundle}"/>" class="btn btn-primary btn-block">
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>

		<%--		<c:if test="${not empty errorMessage}">--%>
		<%--			<c:out value="${errorMessage}"></c:out>--%>
		<%--		</c:if>--%>

		<%--		<c:if test="${not empty successfulMessage}">--%>
		<%--			<c:out value="${successfulMessage}"></c:out>--%>
		<%--		</c:if>--%>

	</main>

	<%@ include file="/WEB-INF/view/jspf/footer.jspf" %>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/validation/faculty-validation.js"></script>
</body>
</html>