<%@ include file="/WEB-INF/view/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/view/jspf/directive/taglib.jspf"%>
<html>
<%@ include file="/WEB-INF/view/jspf/head.jspf"%>
<link href="<c:url value="/static/style/subject.css" />" rel="stylesheet"/>
<%--<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/style/subject.css"/>--%>
<body>
	<%@ include file="/WEB-INF/view/jspf/header.jspf"%>

	<main class="edit-subject">
		<div class="cotainer">
			<div class="row justify-content-center">
				<div class="col-md-8">
					<div class="card">
						<div class="card-header">
							<div class="header">
								<fmt:message key="subject.edit_jsp.label.title" bundle="${langBundle}"/>
							</div>
						</div>
						<div class="card-body">
							<form id="edit_subject" method="POST" action="controller" autocomplete="off">
								<input type="hidden" name="command" value="editSubject">
								<input type="hidden" name="oldName" value="${name_eng}">
								<div class="form-group row">
									<label for="name_ru" class="col-md-4 col-form-label text-md-right">
										<fmt:message key="subject.edit_jsp.label.name" bundle="${langBundle}"/> (ru)
									</label>
									<div class="col-md-6">
										<input type="text" name="name_ru" id="name_ru" value="${name_ru}" class="form-control" required>
									</div>
								</div>
								<div class="form-group row">
									<label for="name_eng" class="col-md-4 col-form-label text-md-right">
										<fmt:message key="subject.edit_jsp.label.name" bundle="${langBundle}"/> (eng)
									</label>
									<div class="col-md-6">
										<input type="text" name="name_eng" id="name_eng" value="${name_eng}" class="form-control" required>
									</div>
								</div>
								<div class="form-group row">
									<div class="col-md-6 offset-md-4">
										<input type="submit" value="<fmt:message key="subject.edit_jsp.button.submit" bundle="${langBundle}"/>" class="btn btn-primary btn-block">
									</div>
								</div>
								<div class="form-group row">
									<div class="col-md-6 offset-md-4">
										<input type="button" onclick="location.href='controller?command=viewSubject&name_eng=${name_eng}';"
											   value="<fmt:message key="subject.edit_jsp.button.back" bundle="${langBundle}"/>" class="btn btn-primary btn-block">
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>

		<%--		<c:if test="${not empty errorMessage}">--%>
		<%--			<c:out value="${errorMessage}"></c:out>--%>
		<%--		</c:if>--%>

		<%--		<c:if test="${not empty successfulMessage}">--%>
		<%--			<c:out value="${successfulMessage}"></c:out>--%>
		<%--		</c:if>--%>

	</main>


	<%@ include file="/WEB-INF/view/jspf/footer.jspf" %>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/validation/subject-validation.js"></script>
</body>
</html>