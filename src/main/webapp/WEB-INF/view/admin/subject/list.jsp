<%@ include file="/WEB-INF/view/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/view/jspf/directive/taglib.jspf"%>
<html>
<%@ include file="/WEB-INF/view/jspf/head.jspf"%>
<link href="<c:url value="/static/style/subject.css" />" rel="stylesheet"/>
<%--<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/style/subject.css"/>--%>
<body>
	<%@ include file="/WEB-INF/view/jspf/header.jspf"%>

	<main class="admin-subjects-list">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-8">
					<div class="card">
						<div class="card-header">
							<div class="header">
								<fmt:message key="subject.list_jsp.label.subjects_list" bundle="${langBundle}"/>
							</div>
						</div>
						<div class="card-body">
							<div class="form-group row">
								<div class="col-md-6 offset-md-4">
									<ol>
										<c:forEach var="subject" items="${allSubjects}">
											<li>
												<a href="<c:url value="controller?command=viewSubject">
											<c:param name="name_eng" value="${subject.nameEng}"/></c:url>">
													<c:out value="${lang eq 'ru' ? subject.nameRu : subject.nameEng}"></c:out>
												</a>
											</li>
										</c:forEach>
									</ol>
								</div>
							</div>
							<div class="form-group row">
								<div class="col-md-6 offset-md-4">
									<input type="button" onclick="location.href='controller?command=addSubject';"
										   value="<fmt:message key="subject.list_jsp.button.add" bundle="${langBundle}"/>" class="btn btn-primary">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</main>

	<%@ include file="/WEB-INF/view/jspf/footer.jspf" %>
</body>
</html>