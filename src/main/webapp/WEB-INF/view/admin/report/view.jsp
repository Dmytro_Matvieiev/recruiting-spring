<%@ include file="/WEB-INF/view/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/view/jspf/directive/taglib.jspf"%>
<html>
<%@ include file="/WEB-INF/view/jspf/head.jspf"%>
<link href="<c:url value="/static/style/report.css" />" rel="stylesheet"/>
<%--<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/style/report.css"/>--%>
<body>
	<%@ include file="/WEB-INF/view/jspf/header.jspf"%>

<%--	<main class="view-report">--%>
<%--		<div class="container">--%>
			<div class="row justify-content-center">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header">
							<div class="header">
								<h4><fmt:message key="report.view_jsp.label.report" bundle="${langBundle}"/> (<c:out value="${lang eq 'ru' ? name_ru : name_eng}"></c:out>)</h4>
							</div>
						</div>
						<div class="card-body">
							<table id="reportTable" class="display">
								<thead>
									<tr>
										<td><fmt:message key="report.view_jsp.label.full_name" bundle="${langBundle}"/></td>
										<td><fmt:message key="report.view_jsp.label.attachment" bundle="${langBundle}"/></td>
										<td><fmt:message key="report.view_jsp.label.email" bundle="${langBundle}"/></td>
										<td><fmt:message key="report.view_jsp.label.isBlocked" bundle="${langBundle}"/></td>
										<td><fmt:message key="report.view_jsp.label.entrance_sum" bundle="${langBundle}"/></td>
										<td><fmt:message key="report.view_jsp.label.diploma_sum" bundle="${langBundle}"/></td>
										<td><fmt:message key="report.view_jsp.label.total_sum" bundle="${langBundle}"/></td>
										<td><fmt:message key="report.view_jsp.label.entered" bundle="${langBundle}"/></td>
										<td><fmt:message key="report.view_jsp.label.entered_on_budget" bundle="${langBundle}"/></td>
									</tr>
								</thead>
								<tbody>
								<c:forEach var="reportRecord" items="${facultyReport}">
									<tr>
										<td><c:out value="${reportRecord.firstName}"></c:out></td>
										<td><c:out value="${reportRecord.lastName}"></c:out></td>
										<td><c:out value="${reportRecord.email}"></c:out></td>
										<td><c:if test="${reportRecord.blockedStatus == true}">
											<fmt:message key="report.view_jsp.label.blocked" bundle="${langBundle}"/>
										</c:if> <c:if test="${reportRecord.blockedStatus == false}">
											<fmt:message key="report.view_jsp.label.unblocked" bundle="${langBundle}"/>
										</c:if></td>
										<td><c:out value="${reportRecord.entranceSum}"></c:out></td>
										<td><c:out value="${reportRecord.diplomaSum}"></c:out></td>
										<td><c:out value="${reportRecord.totalSum}"></c:out></td>
										<td><c:if test="${reportRecord.entered == true}">
											<fmt:message key="report.view_jsp.label.entered_yes_msg" bundle="${langBundle}"/>
										</c:if> <c:if test="${reportRecord.entered == false}">
											<fmt:message key="report.view_jsp.label.entered_no_msg" bundle="${langBundle}"/>
										</c:if></td>
										<td><c:if test="${reportRecord.enteredOnBudget == true}">
											<fmt:message key="report.view_jsp.label.entered_on_budget_yes_msg" bundle="${langBundle}"/>
										</c:if> <c:if test="${reportRecord.enteredOnBudget == false}">
											<fmt:message key="report.view_jsp.label.entered_on_budget_no_msg" bundle="${langBundle}"/>
										</c:if></td>
									</tr>
								</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
<%--		</div>--%>
<%--	</main>--%>




	<script type="text/javascript">
		var language = "${language}";
			$(document).ready(function() {
				$('#reportTable').dataTable({
					destroy: true,
					orderCellsTop: true,
					aaSorting: [],
					lengthMenu: [[5, 10, 25], [5, 10, 25]],
					// "scrollX": true,
					fixedHeader: true,
					dom: 'lBfrtip',
					// "scrollY":        "60vh",
					"scrollCollapse": true,
					ordering: true,
					buttons: [
						'copy', 'excel'
					],
					"language" : {
						"url" : (language == 'ru') ? "../static/js/dataTables/russian.lang" : "",
					}
				});
			});
	</script>

	<%@ include file="/WEB-INF/view/jspf/footer.jspf" %>
</body>
</html>