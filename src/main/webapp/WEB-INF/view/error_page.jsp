<%@ include file="/WEB-INF/view/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/view/jspf/directive/taglib.jspf"%>
<%@ page isErrorPage="true"%>
<%@ page import="java.io.PrintWriter"%>
<html>
<%@ include file="/WEB-INF/view/jspf/head.jspf"%>
<c:set var="title" value="Error" scope="page" />

<body>
<%@ include file="/WEB-INF/view/jspf/header.jspf"%>
<div id="container">

		<div class="content">

			<h5 class="error"><fmt:message key="error.message" bundle="${langBundle}"/></h5>
<%--			<c:set var="code" value="${requestScope['javax.servlet.error.status_code']}" />--%>
<%--			<c:set var="message" value="${requestScope['javax.servlet.error.message']}" />--%>
<%--			<c:if test="${not empty code}">--%>
<%--			<h3>--%>
<%--				Error code: <c:out value="${code}" />--%>
<%--			</h3>--%>
<%--				</c:if> <c:if test="${not empty message}">--%>
<%--			<h3>--%>
<%--				<c:out value="${message}" />--%>
<%--			</h3>--%>
<%--		</c:if>--%>
		</div>

</div>

<%@ include file="/WEB-INF/view/jspf/footer.jspf" %>
</body>
</html>