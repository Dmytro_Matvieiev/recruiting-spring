<%@ include file="/WEB-INF/view/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/view/jspf/directive/taglib.jspf" %>
<html>
<%@ include file="/WEB-INF/view/jspf/head.jspf" %>
<body>

<%@ include file="/WEB-INF/view/jspf/header.jspf" %>

<main class="login-form">
    <div class="cotainer">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <div class="header">
                            <fmt:message key="welcome_jsp.label.greeting" bundle="${langBundle}"/>
                        </div>
                    </div>
                    <div class="card-body">
                        <form id="login_form" action="controller" class="my-form" autocomplete="off" method="POST">
                            <input type="hidden" name="command" value="login"/>

                            <div class="form-group row">
                                <label for="email" class="col-md-4 col-form-label text-md-right">
                                    <fmt:message key="welcome_jsp.label.login" bundle="${langBundle}"/>
                                </label>
                                <div class="col-md-6">
                                    <input type="text" name="email" id="email" class="form-control" required autofocus>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password" class="col-md-4 col-form-label text-md-right">
                                    <fmt:message key="welcome_jsp.label.password" bundle="${langBundle}"/>
                                </label>
                                <div class="col-md-6">
                                    <input type="password" id="password" class="form-control" name="password" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-6 offset-md-4">
                                    <fmt:message key="welcome_jsp.label.not_registered_msg" bundle="${langBundle}"/>
                                    <a href="controller?command=client_registration">
                                        <fmt:message key="welcome_jsp.label.register_here_msg" bundle="${langBundle}"/>
                                    </a>
                                </div>
                            </div>

                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary btn-block">
                                    <fmt:message key="welcome_jsp.button.login" bundle="${langBundle}"/>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <c:if test="${requestScope.signInError ne null}">
        <div class="alert alert-danger" role="alert">
                ${requestScope.signInError}
        </div>
    </c:if>

    <c:if test="${not empty errorMessage}">
        <c:out value="${errorMessage}"></c:out>
    </c:if>

    <c:if test="${not empty successfulMessage}">
        <c:out value="${successfulMessage}"></c:out>
    </c:if>

</main>

<%@ include file="/WEB-INF/view/jspf/footer.jspf" %>
</body>
</html>