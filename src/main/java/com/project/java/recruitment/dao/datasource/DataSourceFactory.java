package com.project.java.recruitment.dao.datasource;

import org.apache.log4j.Logger;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;


/**
 * DataSourceFactory class.
 *
 * @author - Dmytro Matvieiev
 * @version - 1.0
 */
public abstract class DataSourceFactory {

    private static final Logger LOGGER = Logger.getLogger(DataSourceFactory.class);

    private static String driverName = "com.mysql.cj.jdbc.Driver";
    private static String url = "jdbc:mysql://localhost:3306/recruitment";
    private static String username = "root";
    private static String password = "root";

    private DataSourceFactory() {
    }

    public static DataSource getDataSource() {
        try {
            DriverManagerDataSource dataSource = new DriverManagerDataSource();
            dataSource.setDriverClassName(driverName);
            dataSource.setUrl(url);
            dataSource.setUsername(username);
            dataSource.setPassword(password);
            return dataSource;
        } catch (Exception ex) {
            LOGGER.error("Can'nt get DataSource", ex);
        }
        return null;
    }
}
