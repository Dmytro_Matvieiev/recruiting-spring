package com.project.java.recruitment.dao;

import com.project.java.recruitment.domain.FacultyEnrollee;
import com.project.java.recruitment.utils.Fields;
import com.project.java.recruitment.utils.SQLQueries;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Repository;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Data access object for enrollees of faculty related entities.
 * @author - Dmytro Matvieiev
 * @version - 1.0
 */
@Repository
public class FacultyEnrolleesDaoImpl extends DBAbstractDao implements FacultyEnrolleesDao {

    private static final Logger LOGGER = Logger.getLogger(FacultyEnrolleesDaoImpl.class);

    @Autowired
    private DriverManagerDataSource driverManagerDataSource;

    @Override
    public boolean insertFacultyEnrollee(FacultyEnrollee fe) {
        try (Connection con = driverManagerDataSource.getConnection();
             PreparedStatement pstmt = con.prepareStatement(
                     SQLQueries.INSERT_NEW_FACULTY_ENROLLEE, Statement.RETURN_GENERATED_KEYS)) {

            int k = 1;

            pstmt.setInt(k++, fe.getFacultyId());
            pstmt.setInt(k++, fe.getEnrolleeId());

            if (pstmt.executeUpdate() > 0) { //if successfully insert in DB
                return resultSetFacultyEnrollee(pstmt, fe);
            }

        } catch (SQLException ex) {
            LOGGER.error("Can't insert new faculty enrollee", ex);
        }
        return false;
    }

    @Override
    public boolean deleteFacultyEnrollee(FacultyEnrollee fe) {
        try (Connection con = driverManagerDataSource.getConnection();
             PreparedStatement ps = con.prepareStatement(SQLQueries.DELETE_FACULTY_ENROLLEE)) {

            ps.setInt(1, fe.getId());
            return ps.executeUpdate() > 0;
        } catch (SQLException ex) {
            LOGGER.error("Can't delete faculty enrollee", ex);
        }
        return false;
    }

    @Override
    public FacultyEnrollee find(FacultyEnrollee fe) {
        FacultyEnrollee facultyEnrollee = null;
        ResultSet rs = null;
        try (Connection con = driverManagerDataSource.getConnection();
             PreparedStatement pstmt = con.prepareStatement(SQLQueries.FIND_FACULTY_ENROLLEE_BY_FK);) {

            pstmt.setInt(1, fe.getFacultyId());
            pstmt.setInt(2, fe.getEnrolleeId());

            rs = pstmt.executeQuery();
            if (rs.next()) {
                facultyEnrollee = mappingFacultyEnrollee(rs);
            }
            return facultyEnrollee;
        } catch (SQLException ex) {
            LOGGER.error("Can't find faculty enrollee by faculty id: " + fe.getFacultyId() +
                    " and enrollee id: " + fe.getEnrolleeId(), ex);
        } finally {
            close(rs);
        }
        return facultyEnrollee;
    }

    @Override
    public FacultyEnrollee findById(int id) {
        FacultyEnrollee facultyEnrollee = null;
        ResultSet rs = null;
        try (Connection con = driverManagerDataSource.getConnection();
             PreparedStatement pstmt = con.prepareStatement(SQLQueries.FIND_FACULTY_ENROLLEE_BY_ID);) {

            pstmt.setInt(1, id);

            rs = pstmt.executeQuery();
            if (rs.next()) {
                facultyEnrollee = mappingFacultyEnrollee(rs);
            }
            return facultyEnrollee;
        } catch (SQLException ex) {
            LOGGER.error("Can't find faculty enrollee by id: " + id, ex);
        } finally {
            close(rs);
        }
        return facultyEnrollee;
    }

    @Override
    public List<FacultyEnrollee> findAll() {
        List<FacultyEnrollee> facultyEnrollees = new ArrayList<>();
        try (Connection con = driverManagerDataSource.getConnection();
             Statement stmt = con.createStatement();
             ResultSet rs = stmt.executeQuery(SQLQueries.FIND_ALL_FACULTY_ENROLLEE);) {

            while (rs.next()) {
                facultyEnrollees.add(mappingFacultyEnrollee(rs));
            }
        } catch (SQLException ex) {
            LOGGER.error("Can't find all faculty enrollees", ex);
        }
        return facultyEnrollees;
    }

    private FacultyEnrollee mappingFacultyEnrollee(ResultSet rs) {
        FacultyEnrollee facultyEnrollee = new FacultyEnrollee();
        try {
            facultyEnrollee.setId(rs.getInt(Fields.ID));
            facultyEnrollee.setFacultyId(rs.getInt(Fields.FACULTY_FK_ID));
            facultyEnrollee.setEnrolleeId(rs.getInt(Fields.ENROLLEE_FK_ID));
        } catch (SQLException ex) {
            LOGGER.error("Can't mapping ResultSet for facultyEnrollee", ex);
        }
        return facultyEnrollee;
    }

    private boolean resultSetFacultyEnrollee(PreparedStatement pstmt, FacultyEnrollee fe) {
        try (ResultSet rs = pstmt.getGeneratedKeys()) { //get id of new row in DB
            if (rs.next()) {
                fe.setId(rs.getInt(1));
                return true;
            }
        } catch (SQLException ex) {
            LOGGER.error("Can't close result set in resultSetFacultyEnrollee method", ex);
        }
        return false;
    }

}
