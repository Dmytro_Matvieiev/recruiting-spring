//package com.project.java.recruitment.dao.factory;
//
//import com.project.java.recruitment.dao.EnrolleeDao;
//import com.project.java.recruitment.dao.FacultyDao;
//import com.project.java.recruitment.dao.FacultyEnrolleesDao;
//import com.project.java.recruitment.dao.FacultySubjectsDao;
//import com.project.java.recruitment.dao.GradeDao;
//import com.project.java.recruitment.dao.SheetReportDao;
//import com.project.java.recruitment.dao.SubjectDao;
//import com.project.java.recruitment.dao.UserDao;
//
///**
// * Factory for Data Access Objects.
// * @author - Dmytro Matvieiev
// * @version - 1.0
// */
//public abstract class DaoFactory {
//    public static DaoFactory getFactoryByName() {
//        return MySQLDaoFactory.getInstance();
//    }
//
//    public abstract FacultyDao getFacultyDao();
//    public abstract FacultyEnrolleesDao getFacultyEnrolleesDao();
//    public abstract FacultySubjectsDao getFacultySubjectsDao();
//    public abstract UserDao getUserDao();
//    public abstract EnrolleeDao getEnrolleeDao();
//    public abstract SubjectDao getSubjectDao();
//    public abstract GradeDao getGradeDao();
//    public abstract SheetReportDao getSheetReportDao();
//}
