package com.project.java.recruitment.dao;

import com.project.java.recruitment.domain.Faculty;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Repository;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import static com.project.java.recruitment.utils.Fields.FACULTY_BUDGET_PLACES;
import static com.project.java.recruitment.utils.Fields.FACULTY_NAME_ENG;
import static com.project.java.recruitment.utils.Fields.FACULTY_NAME_RU;
import static com.project.java.recruitment.utils.Fields.FACULTY_TOTAL_PLACES;
import static com.project.java.recruitment.utils.Fields.ID;
import static com.project.java.recruitment.utils.SQLQueries.DELETE_FACULTY;
import static com.project.java.recruitment.utils.SQLQueries.FIND_ALL_FACULTIES;
import static com.project.java.recruitment.utils.SQLQueries.FIND_FACULTY_BY_ID;
import static com.project.java.recruitment.utils.SQLQueries.FIND_FACULTY_BY_NAME;
import static com.project.java.recruitment.utils.SQLQueries.INSERT_NEW_FACULTY;
import static com.project.java.recruitment.utils.SQLQueries.UPDATE_FACULTY;

/**
 * Data access object for faculty related entities.
 * @author - Dmytro
 * @version - 1.0
 */
@Repository
public class FacultyDaoImpl extends DBAbstractDao implements FacultyDao {

    private static final Logger LOGGER = Logger.getLogger(FacultyDaoImpl.class);

    @Autowired
    private DriverManagerDataSource driverManagerDataSource;

    @Override
    public List<Faculty> findAllFaculty() {
        List<Faculty> faculties = new ArrayList<>();
        try (Connection con = driverManagerDataSource.getConnection();
             Statement stmt = con.createStatement();
             ResultSet rs = stmt.executeQuery(FIND_ALL_FACULTIES);) {

            while (rs.next()) {
                faculties.add(mappingFaculty(rs));
            }
        } catch (SQLException ex) {
            LOGGER.error("Can't find all faculties", ex);
        }
        return faculties;
    }



    @Override
    public boolean insertFaculty(Faculty f) {
        try (Connection con = driverManagerDataSource.getConnection();
             PreparedStatement pstmt = con.prepareStatement(INSERT_NEW_FACULTY, Statement.RETURN_GENERATED_KEYS)) {

            int k = 1;
            pstmt.setString(k++, f.getNameRu());
            pstmt.setString(k++, f.getNameEng());
            pstmt.setInt(k++, f.getTotalPlaces());
            pstmt.setInt(k++, f.getBudgetPlaces());

            if (pstmt.executeUpdate() > 0) { //if successfully insert in DB
                return resultSetFaculty(pstmt, f);
            }

        } catch (SQLException ex) {
            LOGGER.error("Can't insert new faculty", ex);
        }
        return false;
    }

    @Override
    public boolean updateFaculty(Faculty f) {
        try (Connection con = driverManagerDataSource.getConnection();
             PreparedStatement pstmt = con.prepareStatement(UPDATE_FACULTY)) {

            pstmt.setString(1, f.getNameRu());
            pstmt.setString(2, f.getNameEng());
            pstmt.setInt(3, f.getTotalPlaces());
            pstmt.setInt(4, f.getBudgetPlaces());
            pstmt.setInt(5, f.getId());

            return pstmt.executeUpdate() > 0; //if successfully update in DB
        } catch (SQLException ex) {
            LOGGER.error("Can't update faculty", ex);
        }
        return false;
    }

    @Override
    public boolean deleteFaculty(int id) {
        try (Connection con = driverManagerDataSource.getConnection();
             PreparedStatement ps = con.prepareStatement(DELETE_FACULTY)) {

            ps.setInt(1, id);
            return ps.executeUpdate() > 0;
        } catch (SQLException ex) {
            LOGGER.error("Can't delete faculty", ex);
        }
        return false;
    }

    @Override
    public Faculty findFacultyByName(String facultyName) {
        Faculty faculty = null;
        ResultSet rs = null;
        try (Connection con = driverManagerDataSource.getConnection();
             PreparedStatement pstmt = con.prepareStatement(FIND_FACULTY_BY_NAME);) {

            pstmt.setString(1, facultyName);
            pstmt.setString(2, facultyName);

            rs = pstmt.executeQuery();
            if (rs.next()) {
                faculty = mappingFaculty(rs);
            }
            return faculty;
        } catch (SQLException ex) {
            LOGGER.error("Can't find faculty by name - " + facultyName, ex);
        } finally {
            close(rs);
        }
        return faculty;
    }

    @Override
    public Faculty findFacultyById(int id) {
        Faculty faculty = null;
        ResultSet rs = null;
        try (Connection con = driverManagerDataSource.getConnection();
             PreparedStatement pstmt = con.prepareStatement(FIND_FACULTY_BY_ID);) {

            pstmt.setInt(1, id);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                faculty = mappingFaculty(rs);
            }
            return faculty;
        } catch (SQLException ex) {
            LOGGER.error("Can't find faculty with id " + id, ex);
        } finally {
            close(rs);
        }
        return faculty;
    }

    private boolean resultSetFaculty(PreparedStatement pstmt, Faculty faculty) {
        try (ResultSet rs = pstmt.getGeneratedKeys()) { //get id of new row in DB
            if (rs.next()) {
                faculty.setId(rs.getInt(1));
                return true;
            }
        } catch (SQLException ex) {
            LOGGER.error("Can't close rs in resulSetFaculty method", ex);
        }
        return false;
    }

    /**
     * Mapping record from faculty table for POJO Faculty.
     * @param rs - instance of ResultSet class.
     * @return - Faculty instance.
     */
    private Faculty mappingFaculty(ResultSet rs) {
        Faculty faculty = new Faculty();
        try {
            faculty.setId(rs.getInt(ID));
            faculty.setNameRu(rs.getString(FACULTY_NAME_RU));
            faculty.setNameEng(rs.getString(FACULTY_NAME_ENG));
            faculty.setTotalPlaces(rs.getInt(FACULTY_TOTAL_PLACES));
            faculty.setBudgetPlaces(rs.getInt(FACULTY_BUDGET_PLACES));
        } catch (SQLException ex) {
            LOGGER.error("Can't mapping ResultSet for faculty", ex);
        }
        return faculty;
    }
}
