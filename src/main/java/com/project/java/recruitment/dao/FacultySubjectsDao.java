package com.project.java.recruitment.dao;

import com.project.java.recruitment.domain.Faculty;
import com.project.java.recruitment.domain.FacultySubject;

import java.util.List;

/**
 * Use Data Access Object (DAO) to abstract and encapsulate data source access.
 * The DAO manages the data source connection to receive and write data.
 * @author - Dmytro
 * @version - 1.0
 */
public interface FacultySubjectsDao {
    boolean insertFacultySubjects(FacultySubject fs);

    /*
     * Default behavior of update operation is to be not used in implementation.
     * This is the case because Faculty Subjects is a compound entity, which
     * have foreign keys of Faculty and Subject entities.
     */
    default void updateFacultySubjects(FacultySubject fs) {
        throw new UnsupportedOperationException(
                "This operation is not supported on compound entity 'faculty_subject'");
    }
    boolean deleteFacultySubjects(FacultySubject fs);
    boolean deleteAllFacultySubjects(Faculty faculty);
    List<FacultySubject> findAll();
    FacultySubject find(int id);
}
