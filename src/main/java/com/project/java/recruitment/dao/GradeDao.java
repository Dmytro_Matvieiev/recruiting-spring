package com.project.java.recruitment.dao;

import com.project.java.recruitment.domain.Grade;

import java.util.List;

/**
 * Use Data Access Object (DAO) to abstract and encapsulate data source access.
 * The DAO manages the data source connection to receive and write data.
 * @author - Dmytro
 * @version - 1.0
 */
public interface GradeDao {
    boolean insertGrade(Grade grade);
    boolean updateGrade(Grade grade);
    boolean deleteGrade(int id);
    List<Grade> findAll();
    Grade find(int id);
}
