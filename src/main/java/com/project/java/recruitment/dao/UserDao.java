package com.project.java.recruitment.dao;

import com.project.java.recruitment.domain.User;

import java.util.List;
/**
 * Use Data Access Object (DAO) to abstract and encapsulate data source access.
 * The DAO manages the data source connection to receive and write data.
 * @author - Dmytro
 * @version - 1.0
 */
public interface UserDao {
    List<User> findAllUsers();
    boolean insertUser(User user);
    boolean updateUser(User user);
    boolean deleteUser(int id);
    User find(String email);
    User find(String email, String password);
    User find(int id);
}
