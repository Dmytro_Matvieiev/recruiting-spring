//package com.project.java.recruitment.dao.factory;
//
//import com.project.java.recruitment.dao.EnrolleeDao;
//import com.project.java.recruitment.dao.EnrolleeDaoImpl;
//import com.project.java.recruitment.dao.FacultyDao;
//import com.project.java.recruitment.dao.FacultyDaoImpl;
//import com.project.java.recruitment.dao.FacultyEnrolleesDao;
//import com.project.java.recruitment.dao.FacultyEnrolleesDaoImpl;
//import com.project.java.recruitment.dao.FacultySubjectsDao;
//import com.project.java.recruitment.dao.FacultySubjectsDaoImpl;
//import com.project.java.recruitment.dao.GradeDao;
//import com.project.java.recruitment.dao.GradeDaoImpl;
//import com.project.java.recruitment.dao.SheetReportDao;
//import com.project.java.recruitment.dao.SheetReportDaoImpl;
//import com.project.java.recruitment.dao.SubjectDao;
//import com.project.java.recruitment.dao.SubjectDaoImpl;
//import com.project.java.recruitment.dao.UserDao;
//import com.project.java.recruitment.dao.UserDaoImpl;
//import com.project.java.recruitment.dao.datasource.DataSourceFactory;
//import org.springframework.stereotype.Component;
//
///**
// * MySQLDaoFactory realization of DAO factory.
// * @author - Dmytro Matvieiev
// * @version - 1.0
// */
//@Component
//public class MySQLDaoFactory extends DaoFactory {
//
//    private static final MySQLDaoFactory INSTANCE = new MySQLDaoFactory();
//
//    private final FacultyDaoImpl facultyDao = new FacultyDaoImpl(
//            DataSourceFactory.getDataSource());
//
//    private final FacultyEnrolleesDaoImpl facultyEnrolleesDao = new FacultyEnrolleesDaoImpl(
//            DataSourceFactory.getDataSource());
//
//    private final FacultySubjectsDaoImpl facultySubjectsDao = new FacultySubjectsDaoImpl(
//            DataSourceFactory.getDataSource());
//
//    private final UserDaoImpl userDao = new UserDaoImpl(
//            DataSourceFactory.getDataSource());
//
//    private final EnrolleeDaoImpl enrolleeDao = new EnrolleeDaoImpl(
//            DataSourceFactory.getDataSource());
//
//    private final SubjectDaoImpl subjectDao = new SubjectDaoImpl(
//            DataSourceFactory.getDataSource());
//
//    private final GradeDaoImpl gradeDao = new GradeDaoImpl(
//            DataSourceFactory.getDataSource());
//
//    private final SheetReportDaoImpl sheetReportDao = new SheetReportDaoImpl(
//            DataSourceFactory.getDataSource());
//
//    private MySQLDaoFactory() {
//    }
//
//    public static MySQLDaoFactory getInstance() {
//        return INSTANCE;
//    }
//
//    @Override
//    public FacultyDao getFacultyDao() {
//        return facultyDao;
//    }
//
//    @Override
//    public FacultyEnrolleesDao getFacultyEnrolleesDao() {
//        return facultyEnrolleesDao;
//    }
//
//    @Override
//    public FacultySubjectsDao getFacultySubjectsDao() {
//        return facultySubjectsDao;
//    }
//
//    @Override
//    public UserDao getUserDao() {
//        return userDao;
//    }
//
//    @Override
//    public EnrolleeDao getEnrolleeDao() {
//        return enrolleeDao;
//    }
//
//    @Override
//    public SubjectDao getSubjectDao() {
//        return subjectDao;
//    }
//
//    @Override
//    public GradeDao getGradeDao() {
//        return gradeDao;
//    }
//
//    @Override
//    public SheetReportDao getSheetReportDao() {
//        return sheetReportDao;
//    }
//}
