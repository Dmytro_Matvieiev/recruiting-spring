package com.project.java.recruitment.domain;

/**
 * POJO for final sheet report.
 * Every report has facultyId, fullName of enrollee, email,
 * status. total grades on diploma and entrance.
 * and budget places.
 *
 * @author - Dmytro Matvieiev
 * @version - 1.0
 */
public class EnrolleeSheetReport {

    private int facultyId;
    private String fullName;
    private String email;
    private boolean isBlocked;
    private int entranceSum;
    private int diplomaSum;
    private int totalSum;

    private boolean entered;
    private boolean enteredOnBudget;

    public EnrolleeSheetReport() {
    }

    public EnrolleeSheetReport (int facultyId, String fullName, String email,
                                boolean isBlocked, int entranceSum, int diplomaSum, int totalSum) {
        this.facultyId = facultyId;
        this.fullName = fullName;
        this.email = email;
        this.isBlocked = isBlocked;
        this.entranceSum = entranceSum;
        this.diplomaSum = diplomaSum;
        this.totalSum = totalSum;
    }

    @Override
    public String toString() {
        return "EnrolleeSheetReport{" +
                "facultyId=" + facultyId +
                ", fullName='" + fullName + '\'' +
                ", email='" + email + '\'' +
                ", isBlocked=" + isBlocked +
                ", entranceSum=" + entranceSum +
                ", diplomaSum=" + diplomaSum +
                ", totalSum=" + totalSum +
                '}';
    }

    public int getFacultyId() {
        return facultyId;
    }

    public void setFacultyId(int facultyId) {
        this.facultyId = facultyId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isBlocked() {
        return isBlocked;
    }

    public void setBlocked(boolean blocked) {
        isBlocked = blocked;
    }

    public int getEntranceSum() {
        return entranceSum;
    }

    public void setEntranceSum(int entranceSum) {
        this.entranceSum = entranceSum;
    }

    public int getDiplomaSum() {
        return diplomaSum;
    }

    public void setDiplomaSum(int diplomaSum) {
        this.diplomaSum = diplomaSum;
    }

    public int getTotalSum() {
        return totalSum;
    }

    public void setTotalSum(int totalSum) {
        this.totalSum = totalSum;
    }

    public boolean isEntered() {
        return entered;
    }

    public void setEntered(boolean entered) {
        this.entered = entered;
    }

    public boolean isEnteredOnBudget() {
        return enteredOnBudget;
    }

    public void setEnteredOnBudget(boolean enteredOnBudget) {
        this.enteredOnBudget = enteredOnBudget;
    }
}
