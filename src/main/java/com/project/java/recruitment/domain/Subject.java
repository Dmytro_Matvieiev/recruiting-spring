package com.project.java.recruitment.domain;

import java.util.Objects;

/**
 * POJO for entity Subject.
 * Every subject has name and id.
 *
 * @author - Dmytro Matvieiev
 * @version - 1.0
 */
public class Subject extends Entity {
    private String nameRu;
    private String nameEng;

    public Subject(String nameRu, String nameEng) {
        this.nameRu = nameRu;
        this.nameEng = nameEng;
    }

    public Subject() {

    }

    public String getNameRu() {
        return nameRu;
    }

    public void setNameRu(String nameRu) {
        this.nameRu = nameRu;
    }

    public String getNameEng() {
        return nameEng;
    }

    public void setNameEng(String nameEng) {
        this.nameEng = nameEng;
    }

    @Override
    public String toString() {
        return "Subject{" +
                "nameRu='" + nameRu + '\'' +
                ", nameEng='" + nameEng + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Subject subject = (Subject) o;
        return Objects.equals(nameRu, subject.nameRu) &&
                Objects.equals(nameEng, subject.nameEng);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nameRu, nameEng);
    }
}


