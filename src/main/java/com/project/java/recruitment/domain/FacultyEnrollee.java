package com.project.java.recruitment.domain;

/**
 * POJO for entity faculties of enrollee.
 * Every faculty has name, number of departments, total places
 * and budget places.
 *
 * @author - Dmytro Matvieiev
 * @version - 1.0
 */
public class FacultyEnrollee extends Entity {
    private int facultyId;
    private int enrolleeId;

    public FacultyEnrollee() {
    }

    public FacultyEnrollee(int facultyId, int enrolleeId) {
        this.facultyId = facultyId;
        this.enrolleeId = enrolleeId;
    }

    public FacultyEnrollee(Faculty faculty, Enrollee enrollee) {
        this(faculty.getId(), enrollee.getId());
    }

    @Override
    public String toString() {
        return "FacultyEnrollee{" +
                "facultyId=" + facultyId +
                ", enrolleeId=" + enrolleeId +
                '}';
    }

    public int getFacultyId() {
        return facultyId;
    }

    public void setFacultyId(int facultyId) {
        this.facultyId = facultyId;
    }

    public int getEnrolleeId() {
        return enrolleeId;
    }

    public void setEnrolleeId(int enrolleeId) {
        this.enrolleeId = enrolleeId;
    }
}
