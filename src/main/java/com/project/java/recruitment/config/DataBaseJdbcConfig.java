package com.project.java.recruitment.config;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

/**
 * DB connection configuration.
 * Create JdbcTemplate beans.
 * @author - Dmytro Matvieiev
 * @version - 1.0
 */
@Configuration
public class DataBaseJdbcConfig {

    private static final Logger LOGGER = Logger.getLogger(DataBaseJdbcConfig.class);

    @Value("${db.mysql.driver-class-name}")
    private String driverName;

    @Value("${db.mysql.jdbc-url}")
    private String url;

    @Value("${db.mysql.username}")
    private String username;

    @Value("${db.mysql.password}")
    private String password;


    @Primary
    @Bean(name = "driverManagerDatasource")
//    @Bean(name = "ds-mysql")
    public DriverManagerDataSource getDriverManagerDatasource() {
        LOGGER.info("-------- MySQL JDBC Connection Testing ------------");
        try {
            Class.forName(driverName);
        } catch (ClassNotFoundException e) {
            LOGGER.error("Where is your MySQL JDBC Driver? : {}", e);
        }

        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        try {
            dataSource.setUrl(url);
            dataSource.setUsername(username);
            dataSource.setPassword(password);
        } catch (Exception ex) {
            LOGGER.error("Error getDriverManagerDatasourceRecruiting : {}", ex);
        }
        return dataSource;
    }

//    @Bean(name = "recruitment-mysql")
//    public JdbcTemplate jdbcTemplate(@Qualifier("driverManagerDatasource") DriverManagerDataSource dataSource) {
//        return new JdbcTemplate(dataSource);
//    }
//
//    @Bean(name = "recruitment-mysql-named")
//    public NamedParameterJdbcTemplate jdbcTemplateNamed(@Qualifier("driverManagerDatasource") DriverManagerDataSource dataSource) {
//        return new NamedParameterJdbcTemplate(dataSource);
//    }
}
