package com.project.java.recruitment.config;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

/**
 * Dispatcher Servlet Initialization.
 * @author - Dmytro Matvieiev
 * @version - 1.1
 */
public class SpringDispatcherServletInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {
    @Override
    protected Class<?>[] getRootConfigClasses() {
        return null;
    }

    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class[]{WebSpringMvcConfig.class};
    }

    @Override
    protected String[] getServletMappings () {
        return new String[]{"/"};
    }
}
