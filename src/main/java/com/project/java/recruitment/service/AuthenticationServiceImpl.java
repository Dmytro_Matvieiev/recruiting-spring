package com.project.java.recruitment.service;

import com.project.java.recruitment.dao.UserDao;
import com.project.java.recruitment.domain.User;
import com.project.java.recruitment.service.exception.SignInException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class AuthenticationServiceImpl implements AuthenticationService {

    private static final Logger LOGGER = Logger.getLogger(AuthenticationServiceImpl.class);

    private final UserDao userDao;

    @Autowired
    public AuthenticationServiceImpl(UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public User signIn(String email, String password) {
        LOGGER.info("Starting to sign in");
        if (Objects.isNull(email) || Objects.isNull(password))
            throw new SignInException("Please fill all required fields of the form!");

        return userDao.find(email, password);
    }
}
