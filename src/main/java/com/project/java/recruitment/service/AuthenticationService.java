package com.project.java.recruitment.service;

import com.project.java.recruitment.domain.User;

public interface AuthenticationService {
    User signIn(String email, String password);
}
