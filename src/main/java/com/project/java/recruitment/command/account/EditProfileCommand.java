package com.project.java.recruitment.command.account;

import com.project.java.recruitment.Path;
import com.project.java.recruitment.command.Command;
import com.project.java.recruitment.dao.*;
import com.project.java.recruitment.domain.Enrollee;
import com.project.java.recruitment.domain.User;
import com.project.java.recruitment.utils.Fields;
import com.project.java.recruitment.utils.RequestType;
import com.project.java.recruitment.validation.AccountFormValidation;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


/**
 * Edit users profile command.
 *
 * @author - Dmytro Matvieiev
 * @version - 1.0
 */
@Service
public class EditProfileCommand extends Command {

    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = Logger.getLogger(EditProfileCommand.class);

    @Autowired
    private UserDao userDao;

    @Autowired
    private EnrolleeDao enrolleeDao;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response, RequestType requestType)
            throws IOException, ServletException {
        LOGGER.debug("Start executing edit profile command");

        String result = null;

        if (RequestType.GET == requestType) {
            result = doGet(request, response);
        } else if (RequestType.POST == requestType) {
            result = doPost(request, response);
        }

        LOGGER.debug("Finished executing edit profile command");
        return result;
    }

    /**
     * The page where the user can edit his profile.
     *
     * @return path to the edit profile page.
     */
    private String doGet(HttpServletRequest request, HttpServletResponse response) {
        String result = null;
        HttpSession session = request.getSession(false);

        String userEmail = String.valueOf(session.getAttribute("user"));
        String role = String.valueOf(session.getAttribute("userRole"));

        User user = userDao.find(userEmail);

        request.setAttribute(Fields.USER_FULL_NAME, user.getFullName());
        request.setAttribute(Fields.USER_EMAIL, user.getEmail());
        request.setAttribute(Fields.USER_PASS, user.getPassword());
        request.setAttribute(Fields.USER_LANG, user.getLang());


        if ("client".equals(role)) {
            Enrollee enrollee = enrolleeDao.findEnrollee(user);

            request.setAttribute(Fields.ENROLLEE_CITY, enrollee.getCity());
            request.setAttribute(Fields.ENROLLEE_REGION, enrollee.getRegion());
            request.setAttribute(Fields.ENROLLEE_EDUCATIONAL_INSTITUTION, enrollee.getEducationalInstitution());
            request.setAttribute(Fields.ENROLLEE_ATTACHMENT, enrollee.getAttachment());
            request.setAttribute(Fields.ENROLLEE_IS_BLOCKED, enrollee.isBlocked());

            result = Path.FORWARD_CLIENT_PROFILE_EDIT;
        } else if ("admin".equals(role)) {
            result = Path.FORWARD_ADMIN_PROFILE_EDIT;
        }
        return result;
    }

    /**
     * Invoked when user already edit his profile and wants to update it.
     *
     * @return path to the user profile if command succeeds, otherwise
     *         redisplays editing page.
     */
    private String doPost(HttpServletRequest request, HttpServletResponse response) {
        String oldUserEmail = request.getParameter("oldEmail");
        LOGGER.trace("Fetch request parameter: 'oldEmail' = " + oldUserEmail);

        String userFullName = request.getParameter(Fields.USER_FULL_NAME);
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        String language = request.getParameter("lang");

        boolean valid = AccountFormValidation.validateUserParameters(userFullName, email, password, language);

        HttpSession session = request.getSession(false);
        String role = String.valueOf(session.getAttribute("userRole"));

        String result = null;

        if (valid) {
            if ("admin".equals(role)) {

                User user = userDao.find(oldUserEmail);

                LOGGER.trace("User found with email:" + user);

                user.setFullName(userFullName);
                user.setEmail(email);
                user.setPassword(password);
                user.setLang(language);

                LOGGER.trace("After calling setters with request parameters on user entity: "
                        + user);

                userDao.updateUser(user);

                LOGGER.trace("User info updated");

                // update session attributes if user changed it
                session.setAttribute("user", email);
                session.setAttribute(Fields.USER_LANG, language);

                result = Path.REDIRECT_TO_PROFILE;

            } else if ("client".equals(role)) {
                // if user role is client then we should also update enrollee record for him
                String educationalInstitution = request.getParameter(Fields.ENROLLEE_EDUCATIONAL_INSTITUTION);
                String region = request.getParameter(Fields.ENROLLEE_REGION);
                String city = request.getParameter(Fields.ENROLLEE_CITY);
                boolean blockedStatus = Boolean.parseBoolean(request.getParameter(Fields.ENROLLEE_IS_BLOCKED));

                valid = AccountFormValidation.validateEnrolleeParameters(city,
                        region, educationalInstitution);
                if (valid) {

                    User user = userDao.find(oldUserEmail);

                    LOGGER.trace("User found with email:" + user);

                    user.setFullName(userFullName);
                    user.setEmail(email);
                    user.setPassword(password);
                    user.setLang(language);

                    userDao.updateUser(user);
                    LOGGER.trace("User info updated");

                    Enrollee enrollee = enrolleeDao.findEnrollee(user);

                    enrollee.setCity(city);
                    enrollee.setRegion(region);
                    enrollee.setEducationalInstitution(educationalInstitution);
                    enrollee.setBlocked(blockedStatus);

                    enrolleeDao.updateEnrollee(enrollee);
                    LOGGER.trace("Enrollee info updated");


                    session.setAttribute("user", email); // update session attributes if user changed it
                    session.setAttribute(Fields.USER_LANG, language);

                    result = Path.REDIRECT_TO_PROFILE;
                } else {
                    request.setAttribute("errorMessage", "Please fill all fields properly!");
                    LOGGER.error("errorMessage: Not all fields are properly filled");
                    result = Path.REDIRECT_EDIT_PROFILE;
                }
            }
        } else {
            request.setAttribute("errorMessage", "Please fill all fields properly!");
            LOGGER.error("errorMessage: Not all fields are properly filled");
            result = Path.REDIRECT_EDIT_PROFILE;
        }
            return result;
    }
}
