package com.project.java.recruitment.command.account;

import com.project.java.recruitment.Path;
import com.project.java.recruitment.command.Command;
import com.project.java.recruitment.dao.EnrolleeDao;
import com.project.java.recruitment.dao.UserDao;
import com.project.java.recruitment.domain.Enrollee;
import com.project.java.recruitment.domain.User;
import com.project.java.recruitment.utils.RequestType;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


/**
 * View user profile command.
 *
 * @author - Dmytro Matvieiev
 * @version - 1.0
 */
@Service
public class ViewProfileCommand extends Command {

    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = Logger.getLogger(ViewProfileCommand.class);

    @Autowired
    private UserDao userDao;

    @Autowired
    private EnrolleeDao enrolleeDao;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response, RequestType requestType)
            throws IOException, ServletException {
        LOGGER.debug("Start executing view profile command");
        return doGet(request, response);
    }

    /**
     * Forwards user to his profile page, based on his role.
     *
     * @return path to user profile
     */
    private String doGet(HttpServletRequest request, HttpServletResponse response) {
        String result = null;

        HttpSession session = request.getSession(false);
        String userEmail = String.valueOf(session.getAttribute("user"));

        User user = userDao.find(userEmail);

        request.setAttribute("full_name", user.getFullName());
        request.setAttribute("email", user.getEmail());
        request.setAttribute("role", user.getRole());

        String role = user.getRole();

        if ("client".equals(role)) {

            Enrollee enrollee = enrolleeDao.findEnrollee(user);

            request.setAttribute("city", enrollee.getCity());
            request.setAttribute("region", enrollee.getRegion());
            request.setAttribute("attachment", enrollee.getAttachment());
            request.setAttribute("educational_institution", enrollee.getEducationalInstitution());
            request.setAttribute("is_blocked", enrollee.isBlocked());

            result = Path.FORWARD_CLIENT_PROFILE;
        } else if ("admin".equals(role)) {
            result = Path.FORWARD_ADMIN_PROFILE;
        }
        return result;
    }
}
