package com.project.java.recruitment.command.faculty.result;

import com.project.java.recruitment.Path;
import com.project.java.recruitment.command.Command;
import com.project.java.recruitment.dao.FacultyDao;
import com.project.java.recruitment.dao.SheetReportDao;
import com.project.java.recruitment.domain.EnrolleeSheetReport;
import com.project.java.recruitment.domain.Faculty;
import com.project.java.recruitment.utils.Fields;
import com.project.java.recruitment.utils.RequestType;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * Create faculty's report command.
 *
 * @author - Dmytro Matvieiev
 * @version - 1.0
 */
@Service
public class CreateFacultyReportCommand extends Command {

    private static final long serialVersionUID = 1L;

    private static final Logger LOG = Logger.getLogger(CreateFacultyReportCommand.class);

    @Autowired
    private FacultyDao facultyDao;

    @Autowired
    private SheetReportDao sheetReportDao;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response, RequestType requestType)
            throws IOException, ServletException {
        LOG.debug("Start executing create faculty's report command");
        return doGet(request, response);
    }

    private String doGet(HttpServletRequest request, HttpServletResponse response) {

        String id = request.getParameter(Fields.ID);
        int facultyId = Integer.parseInt(id);

        List<EnrolleeSheetReport> report = sheetReportDao.getReport(facultyId);

        Faculty faculty = facultyDao.findFacultyById(facultyId);

        int totalPlaces = faculty.getTotalPlaces();
        int budgetPlaces = faculty.getBudgetPlaces();

        for (int i = 0; i < report.size(); i++) {

            EnrolleeSheetReport sheetReport = report.get(i);

            if ((i < totalPlaces) && !sheetReport.isBlocked()) {
                sheetReport.setEntered(true);

                if (i < budgetPlaces) {
                    sheetReport.setEnteredOnBudget(true);
                } else {
                    sheetReport.setEnteredOnBudget(false);
                }

            } else {
                sheetReport.setEntered(false);
                sheetReport.setEnteredOnBudget(false);
            }
        }

        request.setAttribute(Fields.FACULTY_NAME_RU, faculty.getNameRu());
        request.setAttribute(Fields.FACULTY_NAME_ENG, faculty.getNameEng());
        request.setAttribute("facultyReport", report);

        return Path.FORWARD_REPORT_SHEET_VIEW;
    }
}
