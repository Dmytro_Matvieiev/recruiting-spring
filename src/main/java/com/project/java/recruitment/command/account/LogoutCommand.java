package com.project.java.recruitment.command.account;

import com.project.java.recruitment.command.Command;
import com.project.java.recruitment.utils.RequestType;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


/**
 * Logout command.
 *
 * @author - Dmytro Matvieiev
 * @version - 1.0
 */
@Service
public class LogoutCommand extends Command {

    private static final long serialVersionUID = -2785976616686657267L;

    private static final Logger LOGGER = Logger.getLogger(LogoutCommand.class);

    /**
     * Logout user from portal.
     *
     * @param request
     * @param response
     * @param requestType
     * @return
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response, RequestType requestType)
            throws IOException, ServletException {
        LOGGER.debug("Start executing logout command");

        HttpSession session = request.getSession(false);
        if (session != null) {
            session.invalidate(); //delete all data from session and close session
        }
        LOGGER.debug("Finished executing logout command");
        return "/";
    }
}
