package com.project.java.recruitment.command.subject;

import com.project.java.recruitment.Path;
import com.project.java.recruitment.command.Command;
import com.project.java.recruitment.dao.SubjectDao;
import com.project.java.recruitment.domain.Subject;
import com.project.java.recruitment.utils.Fields;
import com.project.java.recruitment.utils.RequestType;
import com.project.java.recruitment.validation.SubjectInputValidator;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * Edit subject command.
 *
 * @author - Dmytro Matvieiev
 * @version - 1.0
 */
@Service
public class EditSubjectCommand extends Command {

    private static final long serialVersionUID = 1L;
    private static final Logger LOGGER = Logger.getLogger(EditSubjectCommand.class);

    @Autowired
    private SubjectDao subjectDao;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response, RequestType requestType)
            throws IOException, ServletException {
        LOGGER.debug("Start executing edit subject command");

        String result = null;

        if (requestType == RequestType.GET) {
            result = doGet(request, response);
        } else if (requestType == RequestType.POST) {
            result = doPost(request, response);
        }

        return result;
    }

    /**
     * Forwards admin to edit subject page, where admin can update the subject data.
     *
     * @return path to the edit subject page.
     */
    private String doGet(HttpServletRequest request, HttpServletResponse response) {
        String subjectName = request.getParameter(Fields.FACULTY_NAME_ENG);

        Subject subject = subjectDao.findSubjectByName(subjectName);
        request.setAttribute(Fields.SUBJECT_NAME_RU, subject.getNameRu());
        request.setAttribute(Fields.SUBJECT_NAME_ENG, subject.getNameEng());
        return Path.FORWARD_SUBJECT_EDIT_ADMIN;
    }

    /**
     * Updates subject info.
     *
     * @return path to the view of edited subject if all fields were properly
     *         filled, otherwise redisplays edit page.
     */
    private String doPost(HttpServletRequest request, HttpServletResponse response) {

        String oldSubjectName = request.getParameter("oldName"); // get parameters from page

        Subject subject = subjectDao.findSubjectByName(oldSubjectName); // should not be null !

        String newSubjectNameRu = request.getParameter(Fields.SUBJECT_NAME_RU);
        String newSubjectNameEng = request.getParameter(Fields.SUBJECT_NAME_ENG);
        boolean valid = SubjectInputValidator.
                validateParameters(newSubjectNameRu, newSubjectNameEng);

        String result = null;
        if (valid) {
            subject.setNameRu(newSubjectNameRu);
            subject.setNameEng(newSubjectNameEng);

            subjectDao.updateSubject(subject);

            result = Path.REDIRECT_TO_SUBJECT + newSubjectNameEng;
        } else {
            request.setAttribute("errorMessage", "Please fill all fields properly!");
            LOGGER.error("errorMessage: Not all fields are properly filled");
            result = Path.REDIRECT_SUBJECT_EDIT_ADMIN + oldSubjectName;
        }
            return result;
    }
}
