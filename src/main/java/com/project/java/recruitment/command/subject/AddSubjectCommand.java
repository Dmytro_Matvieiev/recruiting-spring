package com.project.java.recruitment.command.subject;

import com.project.java.recruitment.Path;
import com.project.java.recruitment.command.Command;
import com.project.java.recruitment.dao.SubjectDao;
import com.project.java.recruitment.domain.Subject;
import com.project.java.recruitment.utils.RequestType;
import com.project.java.recruitment.validation.SubjectInputValidator;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * Add subject command.
 *
 * @author - Dmytro Matvieiev
 * @version - 1.0
 */
@Service
public class AddSubjectCommand extends Command {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = Logger.getLogger(AddSubjectCommand.class);

    @Autowired
    private SubjectDao subjectDao;

    @Override
    public String execute(HttpServletRequest request,
                          HttpServletResponse response, RequestType requestType)
            throws IOException, ServletException {
        LOGGER.debug("Start executing add subject command");

        String result = null;

        if (requestType == RequestType.GET) {
            result = doGet(request, response);
        } else if (requestType == RequestType.POST) {
            result = doPost(request, response);
        }

        return result;
    }

    /**
     * Forwards admin to add page.
     *
     * @return path to add page
     */
    private String doGet(HttpServletRequest request, HttpServletResponse response) {
        return Path.FORWARD_SUBJECT_ADD_ADMIN;
    }

    /**
     * Adds subject if fields are properly filled, otherwise redisplays add
     * page.
     *
     * @return view of added subject
     */
    private String doPost(HttpServletRequest request, HttpServletResponse response) {
        String nameRu = request.getParameter("name_ru");
        String nameEng = request.getParameter("name_eng");

        boolean valid = SubjectInputValidator.validateParameters(nameRu, nameEng);

        String result = null;
        if (valid) {
            Subject subject = new Subject();
            subject.setNameRu(nameRu);
            subject.setNameEng(nameEng);

            subjectDao.insertSubject(subject);
            result = Path.REDIRECT_TO_SUBJECT + nameEng;
        } else {
            request.setAttribute("errorMessage", "Please fill all fields properly!");
            LOGGER.error("errorMessage: Not all fields are properly filled");
            result = Path.REDIRECT_SUBJECT_ADD_ADMIN;
        }
            return result;
    }
}
