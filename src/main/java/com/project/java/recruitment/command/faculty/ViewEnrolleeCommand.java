package com.project.java.recruitment.command.faculty;

import com.project.java.recruitment.Path;
import com.project.java.recruitment.command.Command;
import com.project.java.recruitment.dao.EnrolleeDao;
import com.project.java.recruitment.dao.UserDao;
import com.project.java.recruitment.domain.Enrollee;
import com.project.java.recruitment.domain.User;
import com.project.java.recruitment.utils.Fields;
import com.project.java.recruitment.utils.RequestType;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * View enrollee's profile command.
 *
 * @author - Dmytro Matvieiev
 * @version - 1.0
 */
@Service
public class ViewEnrolleeCommand extends Command {

    private static final long serialVersionUID = 1L;

    private static final Logger LOG = Logger.getLogger(ViewEnrolleeCommand.class);

    @Autowired
    private UserDao userDao;

    @Autowired
    private EnrolleeDao enrolleeDao;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response, RequestType requestType)
            throws IOException, ServletException {
        LOG.debug("Start executing view enrollee command");

        String result = null;

        if (requestType == RequestType.GET) {
            result = doGet(request, response);
        } else if (requestType == RequestType.POST) {
            result = doPost(request, response);
        }

        return result;
    }

    /**
     * Forwards admin to enrollee profile.
     *
     * @return path to enrollee profile page
     */
    private String doGet(HttpServletRequest request, HttpServletResponse response) {
        int userId = Integer.parseInt(request.getParameter("userId"));

        User user = userDao.find(userId);

        request.setAttribute("full_name", user.getFullName());
        request.setAttribute("email", user.getEmail());
        request.setAttribute("role", user.getRole());

        Enrollee enrollee = enrolleeDao.findEnrollee(user);

        request.setAttribute(Fields.ID, enrollee.getId());
        request.setAttribute(Fields.ENROLLEE_CITY, enrollee.getCity());
        request.setAttribute(Fields.ENROLLEE_REGION, enrollee.getRegion());
        request.setAttribute(Fields.ENROLLEE_EDUCATIONAL_INSTITUTION, enrollee.getEducationalInstitution());
        request.setAttribute(Fields.ENROLLEE_IS_BLOCKED, enrollee.isBlocked());
        return Path.FORWARD_ENROLLEE_PROFILE;
    }

    /**
     * Changes blocked status of enrollee after submitting button in enrollee view.
     *
     * @return redirects to view enrollee page
     */
    private String doPost(HttpServletRequest request, HttpServletResponse response) {
        int enrolleeId = Integer.parseInt(request.getParameter(Fields.ID));

        Enrollee enrollee = enrolleeDao.findEnrollee(enrolleeId);

        boolean updatedBlockedStatus = !enrollee.isBlocked();
        enrollee.setBlocked(updatedBlockedStatus);

        enrolleeDao.updateEnrollee(enrollee);
        return Path.REDIRECT_ENROLLEE_PROFILE + enrollee.getUserId();
    }
}
