package com.project.java.recruitment.command;

import com.project.java.recruitment.command.account.EditProfileCommand;
import com.project.java.recruitment.command.account.LoginCommand;
import com.project.java.recruitment.command.account.LogoutCommand;
import com.project.java.recruitment.command.account.ViewProfileCommand;
import com.project.java.recruitment.command.faculty.AddFacultyCommand;
import com.project.java.recruitment.command.faculty.ApplyFacultyViewCommand;
import com.project.java.recruitment.command.faculty.DeleteFacultyCommand;
import com.project.java.recruitment.command.faculty.EditFacultyCommand;
import com.project.java.recruitment.command.faculty.ViewAllFacultiesCommand;
import com.project.java.recruitment.command.faculty.ViewEnrolleeCommand;
import com.project.java.recruitment.command.faculty.ViewFacultyCommand;
import com.project.java.recruitment.command.faculty.result.CreateFacultyReportCommand;
import com.project.java.recruitment.command.registration.AdminRegistrationCommand;
import com.project.java.recruitment.command.registration.ClientRegistrationCommand;
import com.project.java.recruitment.command.registration.ConfirmRegistrationCommand;
import com.project.java.recruitment.command.subject.AddSubjectCommand;
import com.project.java.recruitment.command.subject.DeleteSubjectCommand;
import com.project.java.recruitment.command.subject.EditSubjectCommand;
import com.project.java.recruitment.command.subject.ViewAllSubjectsCommand;
import com.project.java.recruitment.command.subject.ViewSubjectCommand;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

/**
 * CommandManager class work with all commands from users of portal.
 * @author - Dmytro
 * @version - 1.1
 */
@Component
public class CommandContainer {

    @Autowired
    private LoginCommand loginCommand;
    @Autowired
    private LogoutCommand logoutCommand;
    @Autowired
    private ViewProfileCommand viewProfileCommand;
    @Autowired
    private EditProfileCommand editProfileCommand;
    @Autowired
    private NoCommand noCommand;
    @Autowired
    private ViewAllFacultiesCommand viewAllFacultiesCommand;
    @Autowired
    private ViewFacultyCommand viewFacultyCommand;
    @Autowired
    private ConfirmRegistrationCommand confirmRegistrationCommand;
    @Autowired
    private ClientRegistrationCommand clientRegistrationCommand;
    @Autowired
    private ApplyFacultyViewCommand applyFacultyViewCommand;
    @Autowired
    private AdminRegistrationCommand adminRegistrationCommand;
    @Autowired
    private EditFacultyCommand editFacultyCommand;
    @Autowired
    private AddFacultyCommand addFacultyCommand;
    @Autowired
    private DeleteFacultyCommand deleteFacultyCommand;
    @Autowired
    private AddSubjectCommand addSubjectCommand;
    @Autowired
    private EditSubjectCommand editSubjectCommand;
    @Autowired
    private ViewAllSubjectsCommand viewAllSubjectsCommand;
    @Autowired
    private ViewSubjectCommand viewSubjectCommand;
    @Autowired
    private ViewEnrolleeCommand viewEnrolleeCommand;
    @Autowired
    private CreateFacultyReportCommand createFacultyReportCommand;
    @Autowired
    private DeleteSubjectCommand deleteSubjectCommand;

    private CommandContainer() {
    }

    private static final Logger LOGGER = Logger.getLogger(CommandContainer.class);

    private Map<String, Command> commands = new HashMap<>();

    @PostConstruct
    public void init() {
        // common commands
        commands.put("login", loginCommand);
        commands.put("logout", logoutCommand);
        commands.put("viewProfile", viewProfileCommand);
        commands.put("editProfile", editProfileCommand);
        commands.put("noCommand", noCommand);
        commands.put("viewFaculty", viewFacultyCommand);
        commands.put("viewAllFaculties", viewAllFacultiesCommand);
        commands.put("confirmRegistration", confirmRegistrationCommand);

        // client commands
        commands.put("client_registration", clientRegistrationCommand);
        commands.put("applyFaculty", applyFacultyViewCommand);

        // admin commands
        commands.put("admin_registration", adminRegistrationCommand);
        commands.put("editFaculty", editFacultyCommand);
        commands.put("addFaculty", addFacultyCommand);
        commands.put("deleteFaculty", deleteFacultyCommand);
        commands.put("addSubject", addSubjectCommand);
        commands.put("editSubject", editSubjectCommand);
        commands.put("viewAllSubjects", viewAllSubjectsCommand);
        commands.put("viewSubject", viewSubjectCommand);
        commands.put("viewEnrollee", viewEnrolleeCommand);
        commands.put("createReport", createFacultyReportCommand);
        commands.put("deleteSubject", deleteSubjectCommand);
    }

    /**
     * Returns command object with the given name and path of the resource.
     *
     * @param commandName
     *            Name of the command.
     * @return Command object.
     */
    public Command get(String commandName) {
        if (commandName == null || !commands.containsKey(commandName)) {
            LOGGER.trace("Command not found, with name - " + commandName);
            return commands.get("noCommand");
        }
        return commands.get(commandName);
    }
}



