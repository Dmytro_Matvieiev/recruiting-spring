package com.project.java.recruitment.command.faculty;

import com.project.java.recruitment.Path;
import com.project.java.recruitment.command.Command;
import com.project.java.recruitment.dao.FacultyDao;
import com.project.java.recruitment.domain.Faculty;
import com.project.java.recruitment.utils.RequestType;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;


/**
 * View a list of all faculties command.
 *
 * @author - Dmytro Matvieiev
 * @version - 1.0
 */
@Service
public class ViewAllFacultiesCommand extends Command {

    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = Logger.getLogger(ViewAllFacultiesCommand.class);

    @Autowired
    private FacultyDao facultyDao;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response, RequestType requestType)
            throws IOException, ServletException {
        LOGGER.debug("Start executing view all faculty command");

        return doGet(request, response);
    }

    /**
     * Forward user to page of all faculties. View type depends on the user
     * role.
     *
     * @return to view of all facultues
     */
    private String doGet(HttpServletRequest request, HttpServletResponse response) {
        String result = null;

        List<Faculty> faculties = facultyDao.findAllFaculty();
        request.setAttribute("faculties", faculties);

        HttpSession session = request.getSession(false);
        String role = (String) session.getAttribute("userRole");

        if (role == null || "client".equals(role)) {
            result = Path.FORWARD_FACULTY_VIEW_ALL_CLIENT;
        } else if ("admin".equals(role)) {
            result = Path.FORWARD_FACULTY_VIEW_ALL_ADMIN;
        }

        return result;
    }
}
