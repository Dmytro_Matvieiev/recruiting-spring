package com.project.java.recruitment.command.subject;

import com.project.java.recruitment.Path;
import com.project.java.recruitment.command.Command;
import com.project.java.recruitment.dao.SubjectDao;
import com.project.java.recruitment.domain.Subject;
import com.project.java.recruitment.utils.RequestType;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * View all subjects command.
 *
 * @author - Dmytro Matvieiev
 * @version - 1.0
 */
@Service
public class ViewAllSubjectsCommand extends Command {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = Logger.getLogger(ViewAllSubjectsCommand.class);

    @Autowired
    private SubjectDao subjectDao;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response, RequestType requestType)
            throws IOException, ServletException {
        LOGGER.debug("Start executing view all subjects command");
        return doGet(request, response);
    }

    /**
     * Forwards admin to the view of all subjects.
     *
     * @return path to all subjects view
     */
    private String doGet(HttpServletRequest request, HttpServletResponse response) {
        List<Subject> allSubjects = subjectDao.findAllSubjects();
        request.setAttribute("allSubjects", allSubjects);
        return Path.FORWARD_SUBJECT_VIEW_ALL_ADMIN;
    }
}
