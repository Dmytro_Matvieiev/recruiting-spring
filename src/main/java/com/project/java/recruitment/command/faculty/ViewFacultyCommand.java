package com.project.java.recruitment.command.faculty;

import com.project.java.recruitment.Path;
import com.project.java.recruitment.command.Command;
import com.project.java.recruitment.dao.EnrolleeDao;
import com.project.java.recruitment.dao.FacultyDao;
import com.project.java.recruitment.dao.SubjectDao;
import com.project.java.recruitment.dao.UserDao;
import com.project.java.recruitment.domain.Enrollee;
import com.project.java.recruitment.domain.Entity;
import com.project.java.recruitment.domain.Faculty;
import com.project.java.recruitment.domain.Subject;
import com.project.java.recruitment.domain.User;
import com.project.java.recruitment.utils.Fields;
import com.project.java.recruitment.utils.RequestType;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


/**
 * View faculty command.
 *
 * @author - Dmytro Matvieiev
 * @version - 1.0
 */
@Service
public class ViewFacultyCommand extends Command {

    private static final long serialVersionUID = 1L;

    private static final Logger LOG = Logger.getLogger(ViewFacultyCommand.class);

    @Autowired
    private UserDao userDao;

    @Autowired
    private FacultyDao facultyDao;

    @Autowired
    private EnrolleeDao enrolleeDao;

    @Autowired
    private SubjectDao subjectDao;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response, RequestType requestType)
            throws IOException, ServletException {
        LOG.debug("Start executing view faculty command");
        return doGet(request, response);
    }

    /**
     * Shows page with faculty attributes. Type of action on the page depends on
     * user role.
     *
     * @return path to the view of some faculty.
     */
    private String doGet(HttpServletRequest request, HttpServletResponse response) {
        String facultyNameEng = request.getParameter(Fields.FACULTY_NAME_ENG);

        LOG.trace("Faculty name to look for is equal to: '" + facultyNameEng + "'");

        String result = null;

        Faculty facultyRecord = facultyDao.findFacultyByName(facultyNameEng);

        LOG.trace("Faculty record found: " + facultyRecord);

        request.setAttribute(Fields.ID, facultyRecord.getId());
        request.setAttribute(Fields.FACULTY_NAME_RU, facultyRecord.getNameRu());
        request.setAttribute(Fields.FACULTY_NAME_ENG, facultyRecord.getNameEng());
        request.setAttribute(Fields.FACULTY_TOTAL_PLACES, facultyRecord.getTotalPlaces());
        request.setAttribute(Fields.FACULTY_BUDGET_PLACES, facultyRecord.getBudgetPlaces());


        List<Subject> facultySubjects = subjectDao.findAllFacultySubjects(facultyRecord);
        request.setAttribute("facultySubjects", facultySubjects);

        HttpSession session = request.getSession(false);
        String role = (String) session.getAttribute("userRole");

        if (role == null || "client".equals(role)) {
            result = Path.FORWARD_FACULTY_VIEW_CLIENT;
        } else if ("admin".equals(role)) {
//            EnrolleeDao enrolleeDao = daoFactory.getEnrolleeDao();
            List<Enrollee> enrollees = enrolleeDao.findAllFacultyEnrollee(facultyRecord);

            Map<Enrollee, String> facultyEnrollees = new TreeMap<>(
                    Comparator.comparingInt(Entity::getId));

            for (Enrollee enrollee : enrollees) {
                User user = userDao.find(enrollee.getUserId());
                facultyEnrollees.put(enrollee, user.getFullName());
            }
            request.setAttribute("facultyEnrollees", facultyEnrollees);

            result = Path.FORWARD_FACULTY_VIEW_ADMIN;
        }

        return result;
    }
}
