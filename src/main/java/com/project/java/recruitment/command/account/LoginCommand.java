package com.project.java.recruitment.command.account;

import com.project.java.recruitment.Path;
import com.project.java.recruitment.command.Command;
import com.project.java.recruitment.dao.UserDao;
import com.project.java.recruitment.domain.User;
import com.project.java.recruitment.utils.RequestType;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


/**
 * Login command.
 *
 * @author - Dmytro Matvieiev
 * @version - 1.0
 */
@Service
public class LoginCommand extends Command {

    private static final long serialVersionUID = -3071536593627692473L;

    private static final Logger LOGGER = Logger.getLogger(LoginCommand.class);

    @Autowired
    private UserDao userDao;


    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response, RequestType requestType)
            throws IOException, ServletException {
        LOGGER.debug("Start executing login command");
        return doPost(request, response);
    }

    /**
     * Login user in portal.
     * After successfully login user redirect to view all faculties.
     * @param request
     * @param response
     * @return
     */
    private String doPost(HttpServletRequest request, HttpServletResponse response) {
        String result = null;

        String email = request.getParameter("email");
        String password = request.getParameter("password");

        User user = userDao.find(email, password);

        LOGGER.trace("User found: " + user);
        if (user == null) {
            request.setAttribute("errorMessage", "Can't find user with this login/password");
            LOGGER.error("errorMessage: Can't find user with login - " + email + " and password - " + password);
            result = "/";
        } else if (!user.isActive()) {
            request.setAttribute("errorMessage", "You are not registered!");
            LOGGER.error("errorMessage: User is not registered or did not complete his registration.");
            result = "/";
        } else {
            HttpSession session = request.getSession(true);
            session.setAttribute("user", user.getEmail());
            session.setAttribute("userRole", user.getRole());
            session.setAttribute("lang", user.getLang());
            result = Path.REDIRECT_TO_VIEW_ALL_FACULTIES;
        }
        return result;
    }
}
