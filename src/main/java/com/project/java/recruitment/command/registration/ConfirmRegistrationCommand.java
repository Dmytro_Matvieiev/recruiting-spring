package com.project.java.recruitment.command.registration;

import com.project.java.recruitment.Path;
import com.project.java.recruitment.command.Command;
import com.project.java.recruitment.dao.UserDao;
import com.project.java.recruitment.domain.User;
import com.project.java.recruitment.utils.RequestType;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * Confirm registration command.
 *
 * @author - Dmytro Matvieiev
 * @version - 1.0
 */
@Service
public class ConfirmRegistrationCommand extends Command {

    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = Logger.getLogger(ConfirmRegistrationCommand.class);

    @Autowired
    private UserDao userDao;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response, RequestType requestType)
            throws IOException, ServletException {
        LOGGER.debug("Start executing confirm registration command");

        String result = null;

        if (RequestType.GET == requestType) {
            result = doGet(request, response);
        } else if (RequestType.POST == requestType) {
            result = null;
        }

        LOGGER.debug("Finished executing confirm registration command");

        return result;
    }

    private String doGet(HttpServletRequest request, HttpServletResponse response) {

        String encryptedEmail = request.getParameter("ID");

        LOGGER.trace("Fetch 'ID' parameter from request: " + encryptedEmail);
        String decodedEmail = new String(Base64.getDecoder().decode(encryptedEmail), StandardCharsets.UTF_8);

        LOGGER.trace("Decode 'ID' to following email: " + decodedEmail);

        User user = userDao.find(decodedEmail);

        if (user.getEmail().equals(decodedEmail)) {
            LOGGER.debug("User with not active status found in database.");
            user.setActive(true);
            LOGGER.debug("User active status updated");
            return Path.WELCOME_PAGE;
        } else {
            LOGGER.error("User not found with email: " + decodedEmail);
            return Path.ERROR_PAGE;
        }
    }
}
