package com.project.java.recruitment.command.subject;

import com.project.java.recruitment.Path;
import com.project.java.recruitment.command.Command;
import com.project.java.recruitment.dao.SubjectDao;
import com.project.java.recruitment.domain.Subject;
import com.project.java.recruitment.utils.Fields;
import com.project.java.recruitment.utils.RequestType;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * View subject command.
 *
 * @author - Dmytro Matvieiev
 * @version - 1.0
 */
@Service
public class ViewSubjectCommand extends Command {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = Logger.getLogger(ViewSubjectCommand.class);

    @Autowired
    private SubjectDao subjectDao;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response, RequestType requestType)
            throws IOException, ServletException {
        LOGGER.debug("Start executing view subject command");
        return doGet(request, response);
    }

    /**
     * Forwards admin to the view of some specific subject.
     *
     * @return path to the subject view.
     */
    private String doGet(HttpServletRequest request, HttpServletResponse response) {
        String subjectNameEng = request.getParameter(Fields.SUBJECT_NAME_ENG);

        Subject subject = subjectDao.findSubjectByName(subjectNameEng);

        request.setAttribute(Fields.ID, subject.getId());
        request.setAttribute(Fields.SUBJECT_NAME_RU, subject.getNameRu());
        request.setAttribute(Fields.SUBJECT_NAME_ENG, subject.getNameEng());
        return Path.FORWARD_SUBJECT_VIEW_ADMIN;
    }

}
