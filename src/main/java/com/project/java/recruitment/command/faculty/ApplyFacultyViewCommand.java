package com.project.java.recruitment.command.faculty;

import com.project.java.recruitment.Path;
import com.project.java.recruitment.command.Command;
import com.project.java.recruitment.dao.*;
import com.project.java.recruitment.domain.Enrollee;
import com.project.java.recruitment.domain.Faculty;
import com.project.java.recruitment.domain.FacultyEnrollee;
import com.project.java.recruitment.domain.Grade;
import com.project.java.recruitment.domain.Subject;
import com.project.java.recruitment.domain.User;
import com.project.java.recruitment.utils.Fields;
import com.project.java.recruitment.utils.RequestType;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


/**
 * Applying for admission to the faculty.
 *
 * @author - Dmytro Matvieiev
 * @version - 1.0
 */
@Service
public class ApplyFacultyViewCommand extends Command {

    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = Logger.getLogger(ApplyFacultyViewCommand.class);

    @Autowired
    private UserDao userDao;

    @Autowired
    private FacultyDao facultyDao;

    @Autowired
    private EnrolleeDao enrolleeDao;

    @Autowired
    private GradeDao gradeDao;

    @Autowired
    private SubjectDao subjectDao;

    @Autowired
    private FacultyEnrolleesDao facultyEnrolleesDao;



    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response, RequestType requestType)
            throws IOException, ServletException {

        LOGGER.debug("Start executing apply faculty view command");

        String result = null;

        if (RequestType.GET == requestType) {
            result = doGet(request, response);
        } else if (RequestType.POST == requestType) {
            result = doPost(request, response);
        }

        return result;
    }

    /**
     * Forwards user to apply page of interested faculty.
     *
     * @return path to apply for faculty page
     */
    private String doGet(HttpServletRequest request, HttpServletResponse response) {
        String result = null;
        String facultyNameEng = request.getParameter(Fields.FACULTY_NAME_ENG);

        Faculty faculty = facultyDao.findFacultyByName(facultyNameEng);

        request.setAttribute(Fields.ID, faculty.getId());
        request.setAttribute(Fields.FACULTY_NAME_RU, faculty.getNameRu());
        request.setAttribute(Fields.FACULTY_NAME_ENG, faculty.getNameEng());
        request.setAttribute(Fields.FACULTY_TOTAL_PLACES, faculty.getTotalPlaces());
        request.setAttribute(Fields.FACULTY_BUDGET_PLACES, faculty.getBudgetPlaces());

        List<Subject> facultySubjects = subjectDao.findAllFacultySubjects(faculty);
        request.setAttribute("facultySubjects", facultySubjects);

        List<Subject> allSubjects = subjectDao.findAllSubjects();
        request.setAttribute("allSubjects", allSubjects);

        result = Path.FORWARD_FACULTY_APPLY_CLIENT;
        return result;
    }

    /**
     * @return redirects user to view of applied faculty if applying is
     *         successful, otherwise redisplays this page.
     */
    private String doPost(HttpServletRequest request, HttpServletResponse response) {
        LOGGER.trace("Start processing applying for faculty form");

        HttpSession session = request.getSession(false);
        String email = String.valueOf(session.getAttribute("user"));

        User user = userDao.find(email);
        Enrollee enrollee = enrolleeDao.findEnrollee(user);

        Integer facultyId = Integer.valueOf(request.getParameter(Fields.ID));

        FacultyEnrollee newFacultyEnrollee = new FacultyEnrollee(facultyId, enrollee.getId());
        FacultyEnrollee existingRecord = facultyEnrolleesDao.find(newFacultyEnrollee);

        if (existingRecord != null) {
            LOGGER.trace("User: " + user + " with Enrollee record: " + enrollee +
                    " already applied for faculty with id: " + facultyId);
            return Path.REDIRECT_TO_VIEW_ALL_FACULTIES;
        } else {

            LOGGER.trace("Start extracting data from request in apply faculty view command");

            Map<String, String[]> parameterMap = request.getParameterMap();

            for (String parameterName : parameterMap.keySet()) {

                if (parameterName.endsWith("entrance") || parameterName.endsWith("diploma")) {
                    String[] value = parameterMap.get(parameterName);
                    int gradeValue = Integer.parseInt(value[0]);
                    String[] subjectIdAndExamType = parameterName.split("_");

                    Integer subjectId = Integer.valueOf(subjectIdAndExamType[0]);
                    String examType = subjectIdAndExamType[1];

                    Grade grade = new Grade(subjectId, enrollee.getId(), gradeValue, examType);
                    gradeDao.insertGrade(grade);
                }
            }

            facultyEnrolleesDao.insertFacultyEnrollee(newFacultyEnrollee);

            Faculty faculty = facultyDao.findFacultyById(facultyId);
            return Path.REDIRECT_TO_FACULTY + faculty.getNameEng();
        }
    }
}
