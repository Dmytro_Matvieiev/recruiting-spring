package com.project.java.recruitment.command.registration;

import com.project.java.recruitment.Path;
import com.project.java.recruitment.command.Command;
import com.project.java.recruitment.dao.EnrolleeDao;
import com.project.java.recruitment.dao.FacultySubjectsDao;
import com.project.java.recruitment.dao.SubjectDao;
import com.project.java.recruitment.dao.UserDao;
import com.project.java.recruitment.domain.Role;
import com.project.java.recruitment.domain.User;
import com.project.java.recruitment.utils.Fields;
import com.project.java.recruitment.utils.RequestType;
import com.project.java.recruitment.validation.AccountFormValidation;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



/**
 * Add new admin command.
 * Serves to assign a user the administrator role by another administrator.
 * @author - Dmytro
 * @version - 1.0
 */
@Service
public class AdminRegistrationCommand extends Command {

    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = Logger.getLogger(AdminRegistrationCommand.class);

    @Autowired
    private UserDao userDao;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response, RequestType requestType)
            throws IOException, ServletException {
        LOGGER.debug("Start executing admin registration command");

        String result = null;

        if (requestType == RequestType.GET) {
            result = doGet(request, response);
        } else if (requestType == RequestType.POST) {
            result = doPost(request, response);
        }

        return result;
    }

    /**
     * Forwards user to registration admin page.
     * @return path where lie this page
     */
    private String doGet(HttpServletRequest request, HttpServletResponse response) {
        return Path.FORWARD_ADMIN_REGISTRATION_PAGE;
    }

    /**
     * If validation is successful then admin record will be added in database.
     *
     * @return after registartion will be completed returns path to welcome
     *         page, if not then doGet method will be called.
     */
    private String doPost(HttpServletRequest request, HttpServletResponse response) {
        String email = request.getParameter(Fields.USER_EMAIL);
        String password = request.getParameter(Fields.USER_PASS);
        String fullName = request.getParameter(Fields.USER_FULL_NAME);
        String lang = request.getParameter(Fields.USER_LANG);

        boolean valid = AccountFormValidation.validateUserParameters(fullName, email, password, lang);

        String result = null;

        if (valid) {
            User user = new User(email, password, fullName, Role.ADMIN, lang, false);

            userDao.insertUser(user);
            /**
             * java.util.ServiceConfigurationError: javax.mail.Provider:
             * Provider com.sun.mail.imap.IMAPProvider not a subtype.
             */
//            MailService.sendConfirmationEmail(user); //confirm by email
            request.setAttribute("successfulMessage",
                    "This account was successfully created. Check email and confirm registration.");
            result = Path.REDIRECT_TO_PROFILE;
        } else  {
            request.setAttribute("errorMessage", "Please fill all fields!");
            LOGGER.error("errorMessage: Not all required fields have been filled");
            result = Path.REDIRECT_ADMIN_REGISTRATION_PAGE;
        }

        return result;
    }
}
