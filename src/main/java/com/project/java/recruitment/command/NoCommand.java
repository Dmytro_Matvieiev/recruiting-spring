package com.project.java.recruitment.command;

import com.project.java.recruitment.Path;
import com.project.java.recruitment.utils.RequestType;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * No command invoked when wasn't found command from client (no request).
 *
 * @author - Dmytro Matvieiev
 * @version - 1.0
 */
@Component
public class NoCommand extends Command {

    private static final long serialVersionUID = 8879403039606311780L;

    private static final Logger LOG = Logger.getLogger(NoCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response, RequestType requestType)
            throws IOException, ServletException {
        LOG.debug("No command execution starts");

        String errorMessage = "No such command";
        request.setAttribute("errorMessage", errorMessage);

        LOG.debug("No command execution finished");
        return Path.ERROR_PAGE;
    }
}
