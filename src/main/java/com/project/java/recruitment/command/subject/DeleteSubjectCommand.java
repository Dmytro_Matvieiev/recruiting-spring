package com.project.java.recruitment.command.subject;

import com.project.java.recruitment.Path;
import com.project.java.recruitment.command.Command;
import com.project.java.recruitment.dao.FacultySubjectsDao;
import com.project.java.recruitment.dao.GradeDao;
import com.project.java.recruitment.dao.SubjectDao;
import com.project.java.recruitment.domain.FacultySubject;
import com.project.java.recruitment.domain.Grade;
import com.project.java.recruitment.domain.Subject;
import com.project.java.recruitment.utils.Fields;
import com.project.java.recruitment.utils.RequestType;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * Delete subject command.
 *
 * @author - Dmytro Matvieiev
 * @version - 1.0
 */
@Service
public class DeleteSubjectCommand extends Command {

    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = Logger.getLogger(DeleteSubjectCommand.class);

    @Autowired
    private GradeDao gradeDao;

    @Autowired
    private SubjectDao subjectDao;

    @Autowired
    private FacultySubjectsDao facultySubjectsDao;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response, RequestType requestType)
            throws IOException, ServletException {
        LOGGER.debug("Start executing delete subject command");
        return doPost(request, response);
    }

    /**
     * Redirects user to view of all subjects after submiting a delete button.
     *
     * @return path to view of all subjects if deletion was successful,
     *         otherwise to subject view.
     */
    private String doPost(HttpServletRequest request, HttpServletResponse response) {
        int subjectId = Integer.parseInt(request.getParameter(Fields.ID));

        Subject subjectToDelete = subjectDao.findSubject(subjectId);

        List<FacultySubject> facultySubjects = facultySubjectsDao.findAll();

        facultySubjects
                .removeIf(record -> record.getSubjectId() != subjectToDelete
                        .getId());

        String result = null;

        if (facultySubjects.isEmpty()) {
            List<Grade> grades = gradeDao.findAll();
            grades.removeIf(record -> record.getSubjectId() != subjectToDelete.getId());

            if (grades.isEmpty()) {
                subjectDao.deleteSubject(subjectToDelete.getId());
                result = Path.REDIRECT_TO_VIEW_ALL_SUBJECTS;
            } else {
                result = Path.REDIRECT_TO_SUBJECT + subjectToDelete.getNameEng();
            }

        } else {
            result = Path.REDIRECT_TO_SUBJECT + subjectToDelete.getNameEng();
        }
        return result;
    }
}
