package com.project.java.recruitment.controller;

import com.project.java.recruitment.Path;
import com.project.java.recruitment.command.account.LoginCommand;
import com.project.java.recruitment.domain.User;
import com.project.java.recruitment.service.AuthenticationService;
import com.project.java.recruitment.service.AuthenticationServiceImpl;
import com.project.java.recruitment.service.exception.SignInException;
import com.project.java.recruitment.utils.RequestType;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;

@RestController
public class LoginController {
    private static final Logger LOGGER = Logger.getLogger(LoginController.class);

    @Autowired
    private LoginCommand loginCommand;

    //    private final AuthenticationService authService;
//
//    @Autowired
//    public LoginController(AuthenticationService authService) {
//        this.authService = authService;
//    }
    @Autowired
    private ApplicationContext applicationContext;

    @GetMapping("/")
    public ModelAndView main() {
        return new ModelAndView("welcome");
    }

//    @PostMapping("/controller/login")
//    public void login(HttpServletRequest req, HttpServletResponse res) {
//        try {
//            System.out.println("LOGGGINNNN");
//            loginCommand.execute(req, res, RequestType.POST);
//        } catch (IOException | ServletException e) {
//            LOGGER.error("Can't sign in", e);
//        }
//    }


//    @PostMapping("/login")
//    public String login(@RequestParam("email") String email, @RequestParam("password") String password,
//                        Model model) {
//
//        User user = authService.signIn(email, password);
//        model.addAttribute("user", user);
//        LOGGER.info("User successfully signed in");
//
//        if (user.getRole().equals("admin")) {
//            return "list";
//        } else if (user.getRole().equals("client")) {
//            return "list";
//        }
//        throw new RuntimeException("User with unknown role");
//    }
//
//    @ExceptionHandler(SignInException.class)
//    public ModelAndView signInError(SignInException e) {
//        LOGGER.info("User failed sign in", e);
//        ModelAndView modelAndView = new ModelAndView();
//        modelAndView.addObject("signInError", e.getMessage());
//        modelAndView.setViewName("welcome");
//        return modelAndView;
//    }

}
