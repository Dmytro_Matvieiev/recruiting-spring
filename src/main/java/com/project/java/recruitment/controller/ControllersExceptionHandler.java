package com.project.java.recruitment.controller;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.Arrays;

@ControllerAdvice
public class ControllersExceptionHandler {

    private static final Logger LOGGER = Logger.getLogger(ControllersExceptionHandler.class);

    @ExceptionHandler(RuntimeException.class)
    public String handleRuntimeException(RuntimeException ex) {
        LOGGER.error(String.valueOf(Arrays.asList(ex.getStackTrace())));
        return "error_page";
    }
}
