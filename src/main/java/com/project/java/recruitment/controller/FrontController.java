package com.project.java.recruitment.controller;

import com.project.java.recruitment.Path;
import com.project.java.recruitment.command.Command;
import com.project.java.recruitment.command.CommandContainer;
import com.project.java.recruitment.utils.RequestType;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * Front Controller is a servlet whose task is to delegate
 * business model information and processing of its
 * presentation to subcomponents.
 * @author - Dmytro Matvieiev
 * @version - 1.0
 */
@Controller
@RequestMapping("/controller")
public class FrontController {

    @Autowired
    private CommandContainer commandContainer;

    private static final Logger LOGGER = Logger.getLogger(FrontController.class);

    @GetMapping
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        process(request, response, RequestType.GET);
    }

    @PostMapping
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        process(request, response, RequestType.POST);
    }

    /**
     * Handles all requests coming from the client by executing the specified
     * command name in a request. Implements PRG pattern by checking action type
     * specified by the invoked method.
     *
     * @param request
     * @param response
     * @param requestType
     * @throws IOException
     * @throws ServletException
     * @see RequestType
     */
    private void process(HttpServletRequest request, HttpServletResponse response, RequestType requestType)
            throws IOException, ServletException {

        LOGGER.debug("Start processing in Controller");

        String commandName = request.getParameter("command");
        LOGGER.trace("Request parameter: 'command' = " + commandName);

        Command command = commandContainer.get(commandName);
        LOGGER.trace("Obtained 'command' = " + command);

        String path = command.execute(request, response, requestType);

        if (path == null) {
            LOGGER.trace("Redirect to address = " + path);
            LOGGER.debug("Controller proccessing finished");
            response.sendRedirect(Path.WELCOME_PAGE);
        } else {
            if (requestType == RequestType.GET) {
                LOGGER.trace("Forward to address = " + path);
                LOGGER.debug("Controller proccessing finished");
                RequestDispatcher disp = request.getRequestDispatcher(path);
                disp.forward(request, response);
            } else if (requestType == RequestType.POST) {
                LOGGER.trace("Redirect to address = " + path);
                LOGGER.debug("Controller proccessing finished");
                response.sendRedirect(path);
            }
        }
    }
}
