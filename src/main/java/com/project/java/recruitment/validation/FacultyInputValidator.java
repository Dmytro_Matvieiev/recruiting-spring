package com.project.java.recruitment.validation;

/**
 * Data validation in the form of creation of new faculty.
 * @author - Dmytro Matvieiev
 * @version - 1.0
 */
public class FacultyInputValidator {

    private FacultyInputValidator() {
    }

    /**
     * Validates user input data in fields for faculty.
     * @return <code>true</code> if parameters valid, <code>false</code>
     *         otherwise.
     */
    public static boolean validateParameters(String facultyNameRu, String facultyNameEng,
                                             String facultyBudgetPlaces, String facultyTotalPlaces) {
        if (!FormsFiledValidator.isCyrillicWord(facultyNameRu)
                || !FormsFiledValidator.isLatinWord(facultyNameEng)) {
            return false;
        }

        if (!FormsFiledValidator.isPositiveDecimalNumber(facultyBudgetPlaces,
                facultyTotalPlaces)) {
            return false;
        }

        if (!FormsFiledValidator.isPositive(Long.valueOf(facultyBudgetPlaces),
                Long.valueOf(facultyTotalPlaces))) {
            return false;
        }

        int budget = Integer.parseInt(facultyBudgetPlaces);
        int total = Integer.parseInt(facultyTotalPlaces);

        if (!FormsFiledValidator.checkBudgetLowerTotal(budget, total)) {
            return false;
        }

        return true;
    }
}
