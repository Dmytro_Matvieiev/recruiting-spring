package com.project.java.recruitment.validation;

/**
 * Validation of data when filling out form fields by the admin.
 * @author - Dmytro Matvieiev
 * @version - 1.0
 */
public class SubjectInputValidator {

    private SubjectInputValidator() {
    }

    /**
     * Validates user input data for subject.
     *
     * @return <code>true</code> if parameters valid, <code>false</code>
     *         otherwise.
     */
    public static boolean validateParameters(String subjectNameRu, String subjectNameEng) {
        if (!FormsFiledValidator.isCyrillicWord(subjectNameRu) || !FormsFiledValidator.isLatinWord(subjectNameEng)) {
            return false;
        }
        return true;
    }
}
