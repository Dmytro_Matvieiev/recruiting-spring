package com.project.java.recruitment.filter;

import org.apache.log4j.Logger;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


/**
 * Security filter.
 * Performs authorization of the user to access
 * resources of the application.
 * @author - Dmytro Matvieiev
 * @version - 1.0
 */
@Component
@Order(1)
public class CommandAccessFilter implements Filter {

    private static final Logger LOGGER = Logger.getLogger(CommandAccessFilter.class);

    private final List<String> urls;

    /**
     * Accessible to all users.
     */
    private final Set<String> accessibleCommands;

    /**
     * Accessible only to logged in users.
     */
    private final Set<String> commonCommands;

    /**
     * Accessible only for client.
     */
    private final Set<String> clientCommands;

    /**
     * Accessible only for administrator.
     */
    private final Set<String> adminCommands;


    /**
     * Default constructor.
     */
    public CommandAccessFilter() {
        urls = new ArrayList<>();
        accessibleCommands = new HashSet<>();
        commonCommands = new HashSet<>();
        clientCommands = new HashSet<>();
        adminCommands = new HashSet<>();

        accessibleCommands.add("login");
        accessibleCommands.add("viewFaculty");
        accessibleCommands.add("viewAllFaculties");
        accessibleCommands.add("client_registration");
        accessibleCommands.add("confirmRegistration");

        commonCommands.add("logout"); // common commands
        commonCommands.add("viewProfile");
        commonCommands.add("editProfile");

        clientCommands.add("applyFaculty"); // client commands

        adminCommands.add("admin_registration");  // admin commands
        adminCommands.add("editFaculty");
        adminCommands.add("addFaculty");
        adminCommands.add("deleteFaculty");
        adminCommands.add("addSubject");
        adminCommands.add("editSubject");
        adminCommands.add("viewAllSubjects");
        adminCommands.add("viewSubject");
        adminCommands.add("viewEnrollee");
        adminCommands.add("createReport");
        adminCommands.add("deleteSubject");
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        LOGGER.debug("Start initializing command access filter: " + CommandAccessFilter.class.getSimpleName());
        String avoidURLs = filterConfig.getInitParameter("avoid-urls"); //filter param name
        StringTokenizer token = new StringTokenizer(avoidURLs, ",");

        while (token.hasMoreTokens()) {
            urls.add(token.nextToken());
        }
        LOGGER.debug("Finished initializing command access filter: " + CommandAccessFilter.class.getSimpleName());
    }

    @Override
    public void destroy() {
        LOGGER.debug("Destroying command access filter: " + CommandAccessFilter.class.getSimpleName());
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        LOGGER.debug("CommandAccessFilter doFilter start");

        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;

        String command = req.getParameter("command");

        if (accessibleCommands.contains(command)) {
            chain.doFilter(req, res); // request for accessible url
        } else {
            HttpSession session = req.getSession(false);
            if (session == null || session.getAttribute("user") == null) {
                res.sendError(HttpServletResponse.SC_UNAUTHORIZED);
            } else {
                if (commonCommands.contains(command)) {
                    chain.doFilter(req, res); // Logged-in user found, so just
                } else {

                    if ("client".equals(session.getAttribute("userRole"))
                            && clientCommands.contains(command)) {

                        chain.doFilter(req, res); // Logged-in user found, so just continue request.
                    } else if ("admin".equals(session.getAttribute("userRole"))
                            && adminCommands.contains(command)) {

                        chain.doFilter(req, res); // Logged-in user found, so just continue request.
                    } else {
                        res.sendError(HttpServletResponse.SC_UNAUTHORIZED);
                    }
                }
            }
        }
        LOGGER.debug("CommandAccessFilter doFilter finished");
    }

}
