package com.project.java.recruitment.bpp;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.cglib.proxy.Proxy;
import org.springframework.stereotype.Component;


@Component
public class TimedAnnotationBeanPostProcessor implements BeanPostProcessor {

    private static final Logger LOGGER = Logger.getLogger(TimedAnnotationBeanPostProcessor.class);

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) {

        Class<? extends Object> beanClass = bean.getClass();
        if (beanClass.isAnnotationPresent(Timed.class)) {
            LOGGER.info("Calling bean post processor before init for bean: " + beanName +
                    "class name: " + bean.getClass() + " with annotation @Timed");
        }

        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) {

        Class<? extends Object> beanClass = bean.getClass();

        if (beanClass.isAnnotationPresent(Timed.class)) {
            LOGGER.info("Calling bean post processor before init for bean: " + beanName +
                    "class name: " + beanClass + " with annotation @Timed");

            return Proxy.newProxyInstance(beanClass.getClassLoader(), beanClass.getInterfaces(), (o, method, objects) -> {
                long before = System.nanoTime();
                Object retVal = method.invoke(bean, objects);
                long after = System.nanoTime();
                LOGGER.info("Method worked: " + (after - before) + " nano seconds");
                return retVal;
            });
        }
        return bean;
    }
}
