INSERT INTO faculty (name_ru, name_eng, total_places, budget_places) VALUES ('Механико-математический факультет', 'Faculty of Mechanics and Mathematics', 540, 500);
INSERT INTO faculty (name_ru, name_eng, total_places, budget_places) VALUES ('Факультет общественных наук и международных отношений', 'Faculty of Social Sciences and International Relations', 120, 100);
INSERT INTO faculty (name_ru, name_eng, total_places, budget_places) VALUES ('Факультет прикладной математики', 'Faculty of Applied Mathematics', 480, 400);
INSERT INTO faculty (name_ru, name_eng, total_places, budget_places) VALUES ('Факультет психологии и специального образования', 'Faculty of Psychology and Special Education', 300, 200);
INSERT INTO faculty (name_ru, name_eng, total_places, budget_places) VALUES ('Факультет систем и средств массовой коммуникации', 'Faculty of Systems and Mass Communication', 300, 200);
INSERT INTO faculty (name_ru, name_eng, total_places, budget_places) VALUES ('Факультет украинской и иностранной филологии и искусствоведения', 'Faculty of Ukrainian and foreign philology and art history', 450, 410);
INSERT INTO faculty (name_ru, name_eng, total_places, budget_places) VALUES ('Факультет физики, электроники и компьютерных систем', 'Faculty of Physics, Electronics and Computer Systems', 500, 470);
INSERT INTO faculty (name_ru, name_eng, total_places, budget_places) VALUES ('Юридический факультет', 'Faculty of Law', 300, 180);
INSERT INTO faculty (name_ru, name_eng, total_places, budget_places) VALUES ('Исторический факультет', 'History department', 500, 410);
INSERT INTO faculty (name_ru, name_eng, total_places, budget_places) VALUES ('Факультет экономики', 'Department of Economics', 300, 200);
INSERT INTO faculty (name_ru, name_eng, total_places, budget_places) VALUES ('Химический факультет', 'Chemical faculty', 420, 370);
INSERT INTO faculty (name_ru, name_eng, total_places, budget_places) VALUES ('Физико-технический факультет', 'Physics and Technology Faculty', 600, 500);
INSERT INTO faculty (name_ru, name_eng, total_places, budget_places) VALUES ('Биолого-экологический факультет', 'Faculty of Biology and Ecology', 330, 280);
INSERT INTO faculty (name_ru, name_eng, total_places, budget_places) VALUES ('Факультет медицинских технологий диагностики и реабилитации', 'Faculty of Medical Technologies, Diagnostics and Rehabilitation', 100, 90);